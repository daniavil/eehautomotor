﻿using EEHAutomotor.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Interfaces
{
    public interface ISiniestros
    {
        public List<SiniestroDto> GetSiniestrosDto(int? IdSin = null);
        public Message PutSiniestro(SiniestroInsertDto sDto);
        List<FileDto> GetFilesDto(string id);
        public List<IdValDto> GetListaResponsabilidad();
    }
}
