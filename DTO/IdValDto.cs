﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class IdValDto
    {
        public string value { get; set; }
        public string label { get; set; }
    }
}
