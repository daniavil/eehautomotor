﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class DocSiniestro
    {
        public int IdDocSiniestro { get; set; }
        public string NomDoc { get; set; }
        public string Ruta { get; set; }
        public DateTime FechaCarga { get; set; }
        public int IdSiniestro { get; set; }

        public virtual Siniestro IdSiniestroNavigation { get; set; }
    }
}
