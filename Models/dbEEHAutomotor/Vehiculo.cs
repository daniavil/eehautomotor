﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class Vehiculo
    {
        public Vehiculo()
        {
            AsignacionVehiculo = new HashSet<AsignacionVehiculo>();
            HistoricoExcesoVelocidad = new HashSet<HistoricoExcesoVelocidad>();
            InverseIdVehiculoAntNavigation = new HashSet<Vehiculo>();
            Placa = new HashSet<Placa>();
            Siniestro = new HashSet<Siniestro>();
        }

        public int IdVehiculo { get; set; }
        public string Color { get; set; }
        public int IdMarca { get; set; }
        public string Categoria { get; set; }
        public string ModeloDescrip { get; set; }
        public string VIN { get; set; }
        public string Motor { get; set; }
        public string TarjetaCombustible { get; set; }
        public int? IdConductor { get; set; }
        public int RentaDia { get; set; }
        public int RentaMes { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int? IdVehiculoAnt { get; set; }
        public DateTime? FechaIngresoFlota { get; set; }
        public DateTime? FechaSalidaFlota { get; set; }
        public int? KmIngreso { get; set; }
        public int? KmSalida { get; set; }
        public string TipoLicencia { get; set; }
        public int? IdAseguradora { get; set; }
        public string Combustible { get; set; }

        public virtual Aseguradora IdAseguradoraNavigation { get; set; }
        public virtual Conductor IdConductorNavigation { get; set; }
        public virtual Marca IdMarcaNavigation { get; set; }
        public virtual Vehiculo IdVehiculoAntNavigation { get; set; }
        public virtual ICollection<AsignacionVehiculo> AsignacionVehiculo { get; set; }
        public virtual ICollection<HistoricoExcesoVelocidad> HistoricoExcesoVelocidad { get; set; }
        public virtual ICollection<Vehiculo> InverseIdVehiculoAntNavigation { get; set; }
        public virtual ICollection<Placa> Placa { get; set; }
        public virtual ICollection<Siniestro> Siniestro { get; set; }
    }
}
