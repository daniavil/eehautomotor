﻿using EEHAutomotor.Interfaces;
using EEHAutomotor.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Controllers
{
    public class ErrorController : Controller
    {
        readonly IGenerales _gen;

        public ErrorController(IGenerales generales)
        {
            _gen = generales;
        }


        [Route("Error/{StatusCode}")]
        public IActionResult Error(int statusCode)
        {
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Recurso No Encontrado";
                    break;
                case 500:
                    ViewBag.ErrorMessage = "Error 500 - Internal server error";
                    break;
                default:
                    ViewBag.ErrorMessage = "Error en el sistema, por favor comunicarse con Soporte de Aplicaciones";
                    break;
            }

            _gen.PutError($"Evento Code Error: {statusCode}", this.GetType().Name);

            return View("_Error",new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
