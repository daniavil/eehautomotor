﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class HistoricoExcesoVelocidad
    {
        public Guid IdExceso { get; set; }
        public int IdVehiculo { get; set; }
        public DateTime FechaHoraFin { get; set; }
        public DateTime Duracion { get; set; }
        public int VelocidadExceso { get; set; }
        public string Localizacion { get; set; }
        public int IdConductor { get; set; }
        public string CordXY { get; set; }
        public DateTime FechaHoraCarga { get; set; }

        public virtual Conductor IdConductorNavigation { get; set; }
        public virtual Vehiculo IdVehiculoNavigation { get; set; }
    }
}
