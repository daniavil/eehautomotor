﻿using EEHAutomotor.Controllers;
using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Components
{
    public class VCMenu : ViewComponent
    {
        private readonly IFun _fun;
        public VCMenu(IFun fun)
        {
            _fun = fun;
        }

        public IViewComponentResult Invoke()
        {
            try
            {
                var Usr = _fun.GetUserDecode();

                if (Usr != null)
                {
                    return View(Usr);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }
    }
}
