﻿/*
 ***********JS FLOTA**********
 *Creado Por Daniel Avila - 15/07/2021
 */

function ControlAsigna(page) {

    switch (page) {
        case 1:
            RenderIndex();
            break;
        case 2:
            RenderUpdate();
            break;
        case 3:
            RenderBuscaVehiculoAsig();
            RenderBuscaConductorAsig();
            break;
        case 4:
            RenderCreate();
            break;
        default: null
    }
}

function RenderIndex() {
    RenderTablaAsigna();
}

function RenderCreate() {
    ValidateForm('FrmNewAsigna');
}

function RenderUpdate() {

    var GridObservaciones = $("#GridObservaciones").DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false
    });
    //$('#GridObservaciones').css('display', 'block');
    //GridObservaciones.columns.adjust().draw();


    if ($('input[type=checkbox][name=Entregado]').is(':checked')) {
        $("#dvSalida").show();
    }
    else {
        $("#dvSalida").hide();
    }

    $('input[type=checkbox][name=Entregado]').change(function () {
        if ($(this).is(':checked')) {
            $("#dvSalida").show();
        }
        else {
            $("#dvSalida").hide();
            $("#KmEntrega").val('');
        }
    });

    SavePostAnyValidateForm('FrmEditAsigna', '/Asigna/PutPopAsigna', 0);
}

function RenderTablaAsigna() {
    var GridVehCon = $("#GridVehCon").DataTable({
        language: DefaultLengDatatable,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],
        columns: [
            { data: "IdAsignacion", visible: false },
            { data: "IdVehiculo" },
            { data: "Vin" },
            { data: "Placa" },
            { data: "Motor" },
            { data: "Conductor" },
            { data: "Identidad" },
            { data: "FechaAsignado" },
            { data: "FechaFinaliza" },
            { data: "KmAsigna", visible: false },
            { data: "KmEntrega", visible: false },
            { data: "Entregado" },
            { data: "NomSector" },
            { data: "Ciudad" },
            { data: "IdEncode" }
        ],
        fixedColumns: false,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                }
            },
            {
                extend: 'excel',
                text: 'Excel',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                }
            },
            {
                extend: 'print',
                text: 'Imprimir',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                }
            }
        ]
    });
}


function RenderBuscaVehiculoAsig() {

    var TablaVehiculos = RenderBuscaVehiculo();

    TablaVehiculos.on('click', 'button', function () {
        var data = TablaVehiculos.row($(this).parents('tr')).data();
        //var obj = { dato1: data['idCita'], dato2: data['paciente'], dato3: data['detalle'] };

        $("#IdVehiculo").val(data['IdVehiculo']);
        $("#VinNew").val(data['Vin']);
        $("#MotorNew").val(data['Motor']);
        $("#PlacaNew").val(data['Placa']);
        $("#MarcaNew").val(data['Marca']);
        $("#ModeloNew").val(data['Modelo']);
        $("#ColorNew").val(data['Color']);
    });
}

function RenderBuscaConductorAsig() {
    var GridConductores = RenderBuscaConductor();

    GridConductores.on('click', 'button', function () {
        var data = GridConductores.row($(this).parents('tr')).data();
        $("#IdConductor").val(data['IdConductor']);
        $("#Identidad").val(data['Identidad']);
        $("#Nombre").val(data['Nombre']);
    });
}

