﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class Proceso
    {
        public Proceso()
        {
            Conductor = new HashSet<Conductor>();
        }

        public int IdProceso { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Conductor> Conductor { get; set; }
    }
}
