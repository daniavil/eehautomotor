﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class Aseguradora
    {
        public Aseguradora()
        {
            Vehiculo = new HashSet<Vehiculo>();
        }

        public int IdAseguradora { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Vehiculo> Vehiculo { get; set; }
    }
}
