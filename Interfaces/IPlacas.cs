﻿using EEHAutomotor.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Interfaces
{
    public interface IPlacas
    {
        public List<PlacasDto> GetListaPlacasNuevas();
        public List<PlacasDto> GetListaTodasPlacas();
        public Message PutPlaca(PlacasDto placa);
    }
}
