﻿using EEHAutomotor.Models.dbEEHAutomotor;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class SiniestroDto
    {
        [Key]
        public int IdSiniestro { get; set; } //(int, not null)
        public string IdEncode { get; set; }
        public int IdVehiculo { get; set; } //(int, not null)
        public string Color { get; set; } //(nvarchar(50), null)
        public int IdMarca { get; set; } //(int, not null)
        public string NomMarca { get; set; } //(nvarchar(50), not null)
        public string Categoria { get; set; } //(nvarchar(150), not null)
        public string ModeloDescrip { get; set; } //(nvarchar(150), not null)
        public string VIN { get; set; } //(nvarchar(100), not null)
        public string Motor { get; set; } //(nvarchar(100), null)
        public string CodPlaca { get; set; } //(nvarchar(10), not null)
        public string TarjetaCombustible { get; set; } //(nvarchar(150), null)
        public int IdConductor { get; set; } //(int, null)
        public string Identidad { get; set; } //(nvarchar(20), null)
        public string NomConductor { get; set; } //(nvarchar(200), not null)
        public int? Telefono { get; set; } //(int, null)
        public string Ciudad { get; set; } //(nvarchar(max), null)
        public DateTime? FechaLicenciaVence { get; set; } //(date, null)
        public DateTime FechaOcurrio { get; set; } //(datetime, not null)
        public DateTime FechaCreacion { get; set; } //(date, not null)
        public int idSector { get; set; } //(int, not null)
        public string Sector { get; set; } //(nvarchar(50), not null)
        public string LugarEvento { get; set; }
        public bool ExistenTerceros { get; set; } //(bit, not null)
        public string ExistenTercerosString { get; set; } //(int, null)
        public string DescripDanio { get; set; } //(nvarchar(max), not null)
        public string RelatoSiniestro { get; set; } //(nvarchar(max), not null)
        public decimal CostoReparacion { get; set; } //(decimal(18,2), not null)
        public decimal? CostoTerceros { get; set; } //(decimal(18,2), not null)
        public string Responsabilidad { get; set; } //(nvarchar(50), not null)
        public int VelocidadTransitada { get; set; } //(int, not null)
        public string DatosTercero { get; set; } //(nvarchar(max), not null)
        public string UsuarioCrea { get; set; } //(nvarchar(max), not null)
        public IEnumerable<DocSiniestro> DocsSiniestro { get; set; }
    }
    public class SiniestroInsertDto
    {
        public SiniestroDto Siniestro { get; set; }
        public List<FileDto> Archivos { get; set; }
    }
}
