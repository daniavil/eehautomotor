﻿using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Components
{
    public class VCBuscaConductor : ViewComponent
    {
        private readonly IConductores _conductores;
        public VCBuscaConductor(IConductores conductores)
        {
            _conductores = conductores;
        }

        public IViewComponentResult Invoke()
        {
            var lista = _conductores.GetListaConducDto();
            return View(lista);
        }
    }
}
