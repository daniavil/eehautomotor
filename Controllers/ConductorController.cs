﻿using AutoMapper;
using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Controllers
{
    public class ConductorController : Controller
    {
        readonly IConductores _conductores;
        readonly IGenerales _gen;
        readonly IFun _fun;
        public ConductorController(IConductores conductores, IGenerales generales, IFun fun)
        {
            _conductores = conductores;
            _gen = generales;
            _fun = fun;
        }

        // GET: ConductorController
        public IActionResult Index()
        {
            var listaConduct = _conductores.GetListaConducDto();
            return View(listaConduct);
        }

        // GET: ConductorController/Create
        public IActionResult Create()
        {
            CargaListadoViewData();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ConductorDto dto)
        {
            try
            {
                var archivos = new List<FileDto>();
                foreach (var (f, i) in Request.Form.Files.Select((v, i) => (v, i)))
                {
                    string nom = "File_" + (i + 1).ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    var archivo = _fun.SaveFile(f, 1, nom);
                    if (archivo != null)
                    {
                        archivos.Add(archivo);
                    }
                }
                var Conductor = new ConductorInsertDto();
                Conductor.Conductor = dto;
                Conductor.FotosLicencia = new List<FileDto>(archivos);
                var resp = _conductores.PutConductor(Conductor);
                //return RedirectToAction(nameof(Index));
                return RedirectToAction("Index", "Conductor");
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return View();
            }
        }
        public IActionResult Edit(string id)
        {
            var cDto = _conductores.GetConductorDtoById(id);
            CargaListadoViewData();
            return View(cDto);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PutPopConductor(ConductorDto dto)
        {
            var Conductor = new ConductorInsertDto();
            Conductor.Conductor = dto;
            Conductor.FotosLicencia = null;
            var resp = _conductores.PutConductor(Conductor);
            return new JsonResult(resp);
        }
        public async Task<IActionResult> DownloadFilesConduc(string id)
        {
            var achivos = _conductores.GetFilesDto(id);

            var zipFileMemoryStream = new MemoryStream();

            using (ZipArchive archive = new ZipArchive(zipFileMemoryStream, ZipArchiveMode.Update, leaveOpen: true))
            {
                foreach (var FilePath in achivos)
                {
                    var botFileName = Path.GetFileName(FilePath.Ruta);
                    var entry = archive.CreateEntry(botFileName);
                    using (var entryStream = entry.Open())
                    using (var fileStream = System.IO.File.OpenRead(FilePath.Ruta))
                    {
                        await fileStream.CopyToAsync(entryStream);
                    }
                }
            }

            zipFileMemoryStream.Seek(0, SeekOrigin.Begin);
            return File(zipFileMemoryStream, "application/octet-stream", "Archivos_Asignacion.zip");
        }
        /********************************MÉTODOS*********************************/
        private void CargaListadoViewData()
        {
            List<SelectListItem> Listlicencias = new List<SelectListItem>();
            Listlicencias.AddRange(new SelectList(_gen.GetListaTipoLicencia(), "value", "label"));
            Listlicencias.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["Listlicencia"] = Listlicencias.OrderBy(x => x.Text);

            List<SelectListItem> ListCargo = new List<SelectListItem>();
            ListCargo.AddRange(new SelectList(_gen.GeListaCargo(), "value", "label"));
            ListCargo.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["ListCargo"] = ListCargo.OrderBy(x => x.Text);

            List<SelectListItem> ListProcesos = new List<SelectListItem>();
            ListProcesos.AddRange(new SelectList(_gen.GeListaProceso(), "value", "label"));
            ListProcesos.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["ListProceso"] = ListProcesos.OrderBy(x => x.Text);

            List<SelectListItem> ListSector = new List<SelectListItem>();
            ListSector.AddRange(new SelectList(_gen.GeListaSectores(), "value", "label"));
            ListSector.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["ListSector"] = ListSector.OrderBy(x => x.Text);
        }


    }
}
