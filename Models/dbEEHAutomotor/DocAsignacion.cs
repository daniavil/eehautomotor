﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class DocAsignacion
    {
        public int IdDocAsignacion { get; set; }
        public string NomDoc { get; set; }
        public string Ruta { get; set; }
        public DateTime FechaCarga { get; set; }
        public int IdAsignacion { get; set; }

        public virtual AsignacionVehiculo IdAsignacionNavigation { get; set; }
    }
}
