﻿/*
 ***********JS Generales**********
 *Creado Por Daniel Avila - 18/10/2021
 */

function RenderBuscaVehiculo() {
    var TablaVehiculos = $("#GridVechiculos").DataTable({
        language: DefaultLengDatatable,
        pageLength: 10,
        columns: [
            { data: "IdVehiculo" },
            { data: "Vin" },
            { data: "Motor" },
            { data: "Placa" },
            { data: "Marca" },
            { data: "Modelo" },
            { data: "Color" },
            { data: "PlacaAnterior" },
            {
                data: null,
                render: function (data, type, row, meta) {
                    return '<button class="btn btn-success round btn-sm" data-dismiss="modal">Seleccionar</button>';
                }
            }
        ],
        fixedColumns: false
    });

    return TablaVehiculos;
}

function RenderBuscaConductor() {
    var GridConductores = $("#GridConductores").DataTable({
        language: DefaultLengDatatable,
        pageLength: 10,
        columns: [
            { data: "IdConductor" },
            { data: "Identidad" },
            { data: "Nombre" },
            { data: "Telefono" },
            { data: "Ciudad" },
            { data: "TipoLicencia" },
            { data: "FechaLicenciaVence" },
            { data: "NomCargo" },
            { data: "NomProceso" },
            {
                data: null,
                render: function (data, type, row, meta) {
                    return '<button class="btn btn-success round btn-sm" data-dismiss="modal">Seleccionar</button>';
                }
            }
        ],
        fixedColumns: false
    });

    return GridConductores;
}

