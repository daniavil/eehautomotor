﻿using AutoMapper;
using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using EEHAutomotor.Models.dbEEHAutomotor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EEHAutomotor.Repositories.Vehiculos
{
    public class Vehiculos : IVehiculos
    {
        private readonly EEHAutomotorModel _db;
        private readonly IFun _fun;
        readonly IGenerales _gen;
        public Vehiculos(EEHAutomotorModel mm, IFun fun, IGenerales generales)
        {
            _db = mm;
            _fun = fun;
            _gen = generales;
        }
        public List<VehiculoDto> GetListaVehiculosDto()
        {
            var lista = new List<VehiculoDto>();
            var l = _db.Vehiculo
                .Include(x => x.Placa)
                .Include(x=>x.IdMarcaNavigation)
                .Include(x=> x.IdConductorNavigation)
                .ToList();
            
            l.ForEach(x =>
            {
                lista.Add(new VehiculoDto
                {
                    IdEncode = _fun.EncodeBase64(x.IdVehiculo.ToString()),
                    IdVehiculo = x.IdVehiculo,
                    Placa = x.Placa?.Where(y => y.EsActual).FirstOrDefault()?.Codigo,
                    Marca = x.IdMarcaNavigation.Descripcion,
                    Modelo = x.ModeloDescrip,
                    Vin = x.VIN,
                    Motor = x.Motor,
                    Color = x.Color,
                    PlacaAnterior = string.Join(", ", (x.Placa?.Where(y => !y.EsActual).Select(z => z.Codigo).ToList())),
                    IdConductor = x.IdConductorNavigation?.IdConductor,
                    Conductor = x.IdConductorNavigation?.Nombre,
                    Identidad = x.IdConductorNavigation?.Identidad,
                    FechaIngresoFlota = x.FechaIngresoFlota,
                    KmIngreso = x.KmIngreso,
                    FechaSalidaFlota = x.FechaSalidaFlota,
                    KmSalida = x.KmSalida,
                    Estado = x.Estado
                });
            });

            return lista;
        }
        public VehiculoInsertDto GetVehiculoById(string idVeh)
        {
            var v = _db.Vehiculo
                .Include(x => x.IdMarcaNavigation)
                .Include(x => x.Placa)
                .Where(x => x.IdVehiculo == int.Parse(_fun.DecodeBase64(idVeh)))
                .FirstOrDefault();

            var config = new MapperConfiguration(cfg => cfg.CreateMap<Vehiculo, VehiculoInsertDto>());
            var mapper = new Mapper(config);
            var vDto = mapper.Map<VehiculoInsertDto>(v);
            vDto.IdPlaca = v.Placa?.Where(x => x.EsActual).FirstOrDefault()?.IdPlaca;
            vDto.PlacaCod = v.Placa?.Where(x => x.EsActual).FirstOrDefault()?.Codigo;
            vDto.Marca = v.IdMarcaNavigation.Descripcion;
            vDto.PlacasAnteriores = string.Join(", ", (v.Placa?.Where(y => !y.EsActual).Select(z => z.Codigo).ToList()));

            var va = GetVehiculoAnterior((int?)v.IdVehiculoAnt);

            if (va != null)
            {
                vDto.IdVehiculoAnt = va.IdVehiculoAnt;
                vDto.PlacaAnt = va.PlacaAnt;
                vDto.MarcaAnt = va.MarcaAnt;
                vDto.ModeloAnt = va.MotorAnt;
                vDto.VinAnt = va.VinAnt;
                vDto.MotorAnt = va.MotorAnt;
                vDto.ColorAnt = va.ColorAnt;
            }
            return vDto;
        }
        public List<IdValDto> GetListaColoresVehiculos()
        {
            var lista = _db.Vehiculo
                .Where(y=>y.Color != null)
                .Select(x => new IdValDto { value = x.Color, label = x.Color })
                .Distinct()
                .ToList();
            return lista;
        }
        public List<IdValDto> GetListaCategoriaVehiculos()
        {
            var lista = _db.Vehiculo.Distinct()
                .Where(y => y.Categoria != null)
                .Select(x => new IdValDto { value = x.Categoria, label = x.Categoria })
                .Distinct()
                .ToList();
            return lista;
        }
        public List<IdValDto> GetListaModeloVehiculos()
        {
            var lista = _db.Vehiculo.Distinct()
                .Where(y => y.ModeloDescrip != null)
                .Select(x => new IdValDto { value = x.ModeloDescrip, label = x.ModeloDescrip })
                .Distinct()
                .ToList();
            return lista;
        }
        public List<IdValDto> GeListatMarcaVehiculos()
        {
            var lista = _db.Marca.Distinct()
                .Select(x => new IdValDto { value = x.IdMarca.ToString(), label = x.Descripcion })
                .Distinct()
                .ToList();
            return lista;
        }
        public List<IdValDto> GetListaConductores()
        {
            var lista = _db.Conductor.Distinct()
                .Select(x => new IdValDto { value = x.IdConductor.ToString(), label = x.Nombre })
                .Distinct()
                .ToList();
            return lista;
        }
        public bool GetExisteVehiculo(string tipo, string dato)
        {
            bool resp = true;
            if (tipo == "vin")
            {
                resp = _db.Vehiculo.Where(x => x.VIN.ToUpper().Equals(dato.Trim().ToUpper())).Any();
            }
            else if (tipo == "motor")
            {
                resp = _db.Vehiculo.Where(x => x.Motor.ToUpper().Equals(dato.Trim())).Any();
            }
            else if (tipo == "placa")
            {
                resp = _db.Placa.Where(x => x.Codigo.ToUpper().Equals(dato.Trim())).Any();
            }
            return resp;
        }
        public Message PutVehiculo(VehiculoInsertDto vDto)
        {
            try
            {
                var config = new MapperConfiguration(cfg =>cfg.CreateMap<VehiculoInsertDto, Vehiculo>());
                var mapper = new Mapper(config);
                var vehiculo = mapper.Map<Vehiculo>(vDto);

                using (_db)
                {

                    var listaPlacas = _db.Placa.Where(x => x.IdVehiculo == vehiculo.IdVehiculo).ToList();

                    //actualizar placas existentes a vehiculo
                    if (listaPlacas.Count > 0)
                    {
                        foreach (var p in listaPlacas)
                        {
                            p.EsActual = false;
                            p.IdVehiculo = vDto.IdVehiculo;
                            _db.Entry(p).State = EntityState.Modified;
                        }
                    }
                    //actualizar nueva placa
                    {
                        var p = _db.Placa.Find(vDto.IdPlaca);
                        p.EsActual = true;
                        p.Correlativo = listaPlacas.Count + 1;
                        p.FechaCambio = DateTime.Now;
                        p.IdVehiculo = vehiculo.IdVehiculo;
                        vehiculo.Placa.Add(p);
                    }

                    if (vehiculo.IdVehiculo == 0)
                    {
                        vehiculo.Categoria = vDto.Categoria.ToUpper();
                        vehiculo.ModeloDescrip = vDto.ModeloDescrip.ToUpper();
                        vehiculo.Color = vDto.Color.ToUpper();
                        vehiculo.VIN = vDto.VIN.ToUpper();
                        vehiculo.Motor = vDto.Motor.ToUpper();
                        vehiculo.TarjetaCombustible = vDto.TarjetaCombustible?.ToUpper();


                        vehiculo.Estado = true;
                        vehiculo.FechaRegistro = DateTime.Now;
                        _db.Entry(vehiculo).State = EntityState.Added;
                    }
                    else
                    {
                        //MODIFICAR SOLO ESTOS VALORES
                        vehiculo = _db.Vehiculo.Find(vDto.IdVehiculo);
                        vehiculo.FechaIngresoFlota = vDto.FechaIngresoFlota;
                        vehiculo.KmIngreso = vDto.KmIngreso;
                        vehiculo.IdAseguradora = vDto.IdAseguradora;
                        vehiculo.TarjetaCombustible = vDto.TarjetaCombustible?.ToUpper();
                        vehiculo.RentaDia = vDto.RentaDia;
                        vehiculo.RentaMes = vDto.RentaMes;
                        vehiculo.TipoLicencia = vDto.TipoLicencia;
                        vehiculo.Combustible = vDto.Combustible;
                        vehiculo.Estado = vDto.Estado;
                        vehiculo.FechaSalidaFlota = vDto.FechaSalidaFlota;
                        vehiculo.KmSalida = vDto.KmSalida;
                        vehiculo.IdVehiculoAnt = vDto.IdVehiculoAnt;

                        _db.Entry(vehiculo).State = EntityState.Modified;
                    }

                    
                    if (_db.SaveChanges() > 0)
                    {
                        return new Message { Code = 1, Result = "ok", Msg = "Datos guardados correctamente" };
                    }
                    else
                    {
                        return new Message { Code = 0, Result = "error", Msg = "Error en guardar los datos" };
                    }
                }
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return new Message { Code = 0, Result = "error", Msg = $"Excepción: {ex.GetBaseException().Message}" };
            }
        }
        private VehiculoAnt GetVehiculoAnterior(int? idAnt)
        {
            var lista = new List<VehiculoAnt>();
            var va = _db.Vehiculo
                .Include(x => x.Placa)
                .Include(x => x.IdMarcaNavigation)
                .Include(x => x.IdConductorNavigation)
                .Where(x => x.IdVehiculo == idAnt)
                .FirstOrDefault();

            if (va != null)
            {
                var vadto = new VehiculoAnt
                {
                    IdVehiculoAnt = va.IdVehiculo,
                    PlacaAnt = va.Placa?.FirstOrDefault()?.Codigo,
                    MarcaAnt = va.IdMarcaNavigation.Descripcion,
                    ModeloAnt = va.ModeloDescrip,
                    VinAnt = va.VIN,
                    MotorAnt = va.Motor,
                    ColorAnt = va.Color
                };

                return vadto;
            }
            return null;
        }
        public List<VehiculoDto> GetListaVehiculoConductorNoAsignado()
        {
            var lista = new List<VehiculoDto>();
            var l = _db.Vehiculo
                .Include(x => x.Placa)
                .Include(x => x.IdMarcaNavigation)
                .Where(x => x.IdConductor == (int?)null && x.Estado)
                .ToList();

            l.ForEach(x =>
            {
                lista.Add(new VehiculoDto
                {
                    IdEncode = _fun.EncodeBase64(x.IdVehiculo.ToString()),
                    IdVehiculo = x.IdVehiculo,
                    Placa = x.Placa?.Where(y => y.EsActual).FirstOrDefault()?.Codigo,
                    Marca = x.IdMarcaNavigation.Descripcion,
                    Modelo = x.ModeloDescrip,
                    Vin = x.VIN,
                    Motor = x.Motor,
                    Color = x.Color,
                    PlacaAnterior = string.Join(", ", (x.Placa?.Where(y => !y.EsActual).Select(z => z.Codigo).ToList())),
                    Conductor = null,
                    Identidad = null,
                    Estado = x.Estado
                });
            });

            return lista;
        }
    }
}
