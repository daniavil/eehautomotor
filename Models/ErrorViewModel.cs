using System;

namespace EEHAutomotor.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }

    public class RespMsj
    {
        public string estado { get; set; }
        public string mensaje { get; set; }
    }
}
