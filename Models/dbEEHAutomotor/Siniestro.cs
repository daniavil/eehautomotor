﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class Siniestro
    {
        public Siniestro()
        {
            DocSiniestro = new HashSet<DocSiniestro>();
        }

        public int IdSiniestro { get; set; }
        public int IdVehiculo { get; set; }
        public DateTime FechaOcurrio { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int idSector { get; set; }
        public int IdConductor { get; set; }
        public bool ExistenTerceros { get; set; }
        public string LugarEvento { get; set; }
        public string DescripDanio { get; set; }
        public string RelatoSiniestro { get; set; }
        public decimal CostoReparacion { get; set; }
        public decimal? CostoTerceros { get; set; }
        public string Responsabilidad { get; set; }
        public int VelocidadTransitada { get; set; }
        public string DatosTercero { get; set; }
        public string UsuarioCrea { get; set; }

        public virtual Conductor IdConductorNavigation { get; set; }
        public virtual Vehiculo IdVehiculoNavigation { get; set; }
        public virtual Sector idSectorNavigation { get; set; }
        public virtual ICollection<DocSiniestro> DocSiniestro { get; set; }
    }
}
