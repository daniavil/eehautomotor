﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class VehiculoDto
    {
        public string IdEncode { get; set; }
        [Key]
        public int IdVehiculo { get; set; }
        public string Placa { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Vin { get; set; }
        public string Motor { get; set; }
        public string Color { get; set; }
        public string PlacaAnterior { get; set; }
        public int? IdConductor { get; set; }
        public string Conductor { get; set; }
        public string Identidad { get; set; }
        public DateTime? FechaIngresoFlota { get; set; }
        public int? KmIngreso { get; set; }
        public DateTime? FechaSalidaFlota { get; set; }
        public int? KmSalida { get; set; }
        public bool Estado { get; set; }
    }
    public class VehiculoAnt
    {
        public int IdVehiculoAnt { get; set; }
        public string PlacaAnt { get; set; }
        public string MarcaAnt { get; set; }
        public string ModeloAnt { get; set; }
        public string VinAnt { get; set; }
        public string MotorAnt { get; set; }
        public string ColorAnt { get; set; }
    }
    public class VehiculoInsertDto
    {
        public Message SaveState { get; set; }
        [Key]
        [DefaultValue(null)]
        public int IdVehiculo { get; set; }
        [Required(ErrorMessage = "Campo requerido!")]
        public string Color { get; set; }//
        [Required(ErrorMessage = "Campo requerido!")]
        public int IdMarca { get; set; }//
        [Required(ErrorMessage = "Campo requerido!")]
        public string Marca { get; set; }//
        public string Categoria { get; set; }//
        [Required(ErrorMessage = "Campo requerido!")]
        public string ModeloDescrip { get; set; }//
        [Required(ErrorMessage = "Campo requerido!")]
        public string VIN { get; set; }//
        [Required(ErrorMessage = "Campo requerido!")]
        public string Motor { get; set; }//
        public string TarjetaCombustible { get; set; }//
        public int? IdConductor { get; set; }//
        [Required(ErrorMessage = "Campo requerido!")]
        public int RentaDia { get; set; }//
        [Required(ErrorMessage = "Campo requerido!")]
        public int RentaMes { get; set; }//
        public bool Estado { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int? IdPlaca { get; set; }//
        public string PlacaCod { get; set; }//
        [Required(ErrorMessage = "Campo requerido!")]
        public DateTime? FechaIngresoFlota { get; set; }
        public DateTime? FechaSalidaFlota { get; set; }
        [Required(ErrorMessage = "Campo requerido!")]
        public int? KmIngreso { get; set; }
        public int? KmSalida { get; set; }
        public int? IdAseguradora { get; set; }
        [Required(ErrorMessage = "Campo requerido!")]
        public string Combustible { get; set; }
        public string TipoLicencia { get; set; }
        public string PlacasAnteriores { get; set; }
        //--------------------------------------------------------
        public int? IdVehiculoAnt { get; set; }
        public string VinAnt { get; set; }
        public string MotorAnt { get; set; }
        public string PlacaAnt { get; set; }
        public string MarcaAnt { get; set; }
        public string ModeloAnt { get; set; }
        public string ColorAnt { get; set; }
    }
}
