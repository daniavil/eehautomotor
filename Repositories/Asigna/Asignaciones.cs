﻿using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using EEHAutomotor.Models.dbEEHAutomotor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace EEHAutomotor.Repositories.Asigna
{
    public class Asignaciones : IAsigna
    {
        private readonly EEHAutomotorModel _db;
        private readonly IFun _fun;
        private readonly IVehiculos _vehiculos;
        readonly IGenerales _gen;

        public Asignaciones(EEHAutomotorModel mm, IVehiculos vehiculos, IFun fun, IGenerales generales)
        {
            _db = mm;
            _vehiculos = vehiculos;
            _fun = fun;
            _gen = generales;
        }
        public AsignaDto GetAsignacionByID(string id)
        {
            var a = _db.AsignacionVehiculo
                .Include(x=>x.IdVehiculoNavigation)
                .Include(x => x.IdConductorNavigation)
                .Include(x => x.ObservacionAsignacion)
                .Where(x => x.IdAsignacion == int.Parse(_fun.DecodeBase64(id)))
                .FirstOrDefault();

            var l = _vehiculos.GetListaVehiculosDto();
            var EsActual = l.Any(b => b.IdVehiculo == a.IdVehiculo && b.IdConductor == a.IdConductor);

            var lo = new List<ObservacionDto>();
            foreach (var o in a.ObservacionAsignacion)
            {
                lo.Add(new ObservacionDto
                {
                    Fecha = o.Fecha,
                    Descripcion = o.Descripcion
                });
            }

            var aDto = new AsignaDto
            {
                IdVehiculo = a.IdVehiculo,
                IdConductor = a.IdConductorNavigation.IdConductor,
                Identidad = a.IdConductorNavigation.Identidad,
                Conductor = a.IdConductorNavigation.Nombre,
                IdAsignacion = a.IdAsignacion,
                IdEncode = _fun.EncodeBase64(a.IdAsignacion.ToString()),
                FechaAsignado = a.FechaAsignado,
                FechaFinaliza = a.FechaFinaliza,
                KmAsigna = a.KmAsigna,
                KmEntrega = a.KmEntrega,
                Entregado = a.Entregado,
                Vehiculo = l.Where(y => y.IdVehiculo == a.IdVehiculo).FirstOrDefault(),
                Observaciones = lo
            };
            return aDto;
        }
        public List<AsignaDto> GetListaAsignaciones()
        {
            var lista = new List<AsignaDto>();
            var l = _vehiculos.GetListaVehiculosDto();
            var a = _db.AsignacionVehiculo
                .Include(x=>x.IdConductorNavigation)
                .Include(x => x.IdConductorNavigation.idSectorNavigation)
                .ToList();

            a.ForEach(x => 
            {
                var EsActual = l.Any(b => b.IdVehiculo == x.IdVehiculo && b.IdConductor == x.IdConductor);

                lista.Add(new AsignaDto
                {
                    IdVehiculo = x.IdVehiculo,
                    IdConductor = x.IdConductorNavigation.IdConductor,
                    Identidad = x.IdConductorNavigation.Identidad,
                    Conductor = x.IdConductorNavigation.Nombre,
                    IdAsignacion = x.IdAsignacion,
                    IdEncode = _fun.EncodeBase64(x.IdAsignacion.ToString()),
                    FechaAsignado = x.FechaAsignado,
                    FechaFinaliza = x.FechaFinaliza,
                    KmAsigna = x.KmAsigna,
                    KmEntrega = x.KmEntrega,
                    Entregado = x.Entregado,
                    NomSector = x.IdConductorNavigation.idSectorNavigation?.Nombre,
                    Ciudad = x.IdConductorNavigation.Ciudad,
                    Vehiculo = l.Where(y => y.IdVehiculo == x.IdVehiculo).FirstOrDefault()
                });
            });
            return lista;
        }
        public Message PutAsignacion(AsignaInsertDto aDto)
        {
            try
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<AsignaDto, AsignacionVehiculo>());
                var mapper = new Mapper(config);
                var asigna = mapper.Map<AsignacionVehiculo>(aDto.Asigna);

                using (_db)
                {
                    if (aDto.Archivos != null)
                    {
                        foreach (var a in aDto.Archivos)
                        {
                            var config1 = new MapperConfiguration(cfg => cfg.CreateMap<FileDto, DocAsignacion>());
                            var mapper1 = new Mapper(config1);
                            var arch = mapper1.Map<DocAsignacion>(a);
                            //foto.IdConductor = Conductor.IdConductor;
                            asigna.DocAsignacion.Add(arch);
                        }
                        foreach (var archivo in asigna.DocAsignacion)
                        {
                            _db.Entry(archivo).State = EntityState.Added;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(aDto.Asigna.ObsvInsrt?.Trim()))
                    {
                        asigna.ObservacionAsignacion.Add(new ObservacionAsignacion
                        {
                            Descripcion = aDto.Asigna.ObsvInsrt.Trim(),
                            Fecha = DateTime.Now
                        });
                        foreach (var obs in asigna.ObservacionAsignacion)
                        {
                            _db.Entry(obs).State = EntityState.Added;
                        }
                    }

                    var v = _db.Vehiculo.Find(aDto.Asigna.IdVehiculo);

                    if (asigna.Entregado)
                    {
                        v.IdConductor = (int?)null;
                        asigna.FechaFinaliza = DateTime.Now;
                    }
                    else {
                        v.IdConductor = aDto.Asigna.IdConductor;
                        asigna.FechaFinaliza = (DateTime?)null;
                    }
                    _db.Entry(v).State = EntityState.Modified;
                    _db.Entry(asigna).State = asigna.IdAsignacion == 0 ? EntityState.Added : EntityState.Modified;

                    if (_db.SaveChanges() > 0)
                    {
                        return new Message { Code = 1, Result = "ok", Msg = "Datos guardados correctamente" };
                    }
                    else
                    {
                        return new Message { Code = 0, Result = "error", Msg = "Error en guardar los datos" };
                    }
                }
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return new Message { Code = 0, Result = "error", Msg = $"Excepción: {ex.GetBaseException().Message}" };
            }
        }
        public List<FileDto> GetFilesDto(string id)
        {
            List<FileDto> filesDto = new List<FileDto>();

            var f = _db.DocAsignacion
                .Where(x => x.IdAsignacion == int.Parse(_fun.DecodeBase64(id)))
                .ToList();

            foreach (var item in f)
            {
                var arch = new FileDto
                {
                    NomDoc = item.NomDoc,
                    Ruta = item.Ruta
                };
                filesDto.Add(arch);
            }
            return filesDto;
        }
    }
}
