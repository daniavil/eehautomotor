﻿using EEHAutomotor.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Interfaces
{
    public interface IAsigna
    {
        public List<AsignaDto> GetListaAsignaciones();
        public Message PutAsignacion(AsignaInsertDto aDto);
        public AsignaDto GetAsignacionByID(string id);
        List<FileDto> GetFilesDto(string id);
    }
}
