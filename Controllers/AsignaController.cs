﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EEHAutomotor.Controllers
{
    public class AsignaController : Controller
    {
        readonly IAsigna _asigna;
        readonly IFun _fun;
        readonly IGenerales _gen;
        private readonly IWebHostEnvironment hostEnvironment;
        public AsignaController(IAsigna asigna, IFun fun, IWebHostEnvironment hostEnvironment, IGenerales generales)
        {
            _asigna = asigna;
            _fun = fun;
            _gen = generales;
            this.hostEnvironment = hostEnvironment;
        }

        // GET: AsignaController
        public ActionResult Index()
        {
            var listaAsigna = _asigna.GetListaAsignaciones();
            return View(listaAsigna);
        }

        // GET: AsignaController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AsignaController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AsignaDto dto)
        {
            try
            {
                var archivos = new List<FileDto>();
                foreach (var (f, i) in Request.Form.Files.Select((v, i) => (v, i)))
                {
                    string nom = "File_" + (i + 1).ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    var archivo = _fun.SaveFile(f, 2, nom);
                    if (archivo != null)
                    {
                        archivos.Add(archivo);
                    }
                }

                var asigna = new AsignaInsertDto();
                asigna.Asigna = dto;
                asigna.Archivos= new List<FileDto>(archivos);

                var res = _asigna.PutAsignacion(asigna);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return View();
            }
        }

        public IActionResult Edit(string id)
        {
            var a = _asigna.GetAsignacionByID(id);
            return View(a);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PutPopAsigna(AsignaDto dto)
        {
            var asigna = new AsignaInsertDto();
            asigna.Asigna = dto;
            asigna.Archivos = null;

            var resp = _asigna.PutAsignacion(asigna);
            return new JsonResult(resp);
        }

        public async Task<IActionResult> DownloadFilesAsig(string id)
        {
            var achivos = _asigna.GetFilesDto(id);

            var zipFileMemoryStream = new MemoryStream();

            using (ZipArchive archive = new ZipArchive(zipFileMemoryStream, ZipArchiveMode.Update, leaveOpen: true))
            {
                foreach (var FilePath in achivos)
                {
                    var botFileName = Path.GetFileName(FilePath.Ruta);
                    var entry = archive.CreateEntry(botFileName);
                    using (var entryStream = entry.Open())
                    using (var fileStream = System.IO.File.OpenRead(FilePath.Ruta))
                    {
                        await fileStream.CopyToAsync(entryStream);
                    }
                }
            }

            zipFileMemoryStream.Seek(0, SeekOrigin.Begin);
            return File(zipFileMemoryStream, "application/octet-stream", "Archivos_Asignacion.zip");
        }
    }
}
