﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class Placa
    {
        public int IdPlaca { get; set; }
        public string Codigo { get; set; }
        public int? IdVehiculo { get; set; }
        public bool EsActual { get; set; }
        public int Correlativo { get; set; }
        public DateTime FechaCambio { get; set; }

        public virtual Vehiculo IdVehiculoNavigation { get; set; }
    }
}
