﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class ErrorDto
    {
        public string Mensaje { get; set; }
        public string Ruta { get; set; }
    }
}
