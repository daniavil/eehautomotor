﻿/*
 ***********JS FLOTA**********
 *Creado Por Daniel Avila - 15/07/2021
 */

function ControlPlacas(page) {

    switch (page) {
        case 1:
            RenderIndex();
            break;
        case 2:
            RenderCreate();
            break;
        //case 3:
        //    RenderBuscaVehiculo();
        //    break;
        //case 4:
        //    RenderEditVehiculo();
        //    break;
        //case 5:
        //    RenderBuscaNewPlaca();
        //    break;
        default: null
    }
}

function RenderIndex() {
    RenderTablaPlacas();
}

function RenderCreate() {
    SavePostAnyValidateForm('FrmNewPlaca', '/Placa/CreatePlaca', 1);
}

function RenderTablaPlacas() {
    var GridPlacas = $("#GridPlacas").DataTable({
        language: DefaultLengDatatable,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],
        columns: [
            { data: "Codigo" },
            { data: "IdVehiculo" },
            { data: "Vin" },
            { data: "Motor" },
            { data: "Marca" },
            { data: "Modelo" },
            { data: "Color" },
            { data: "EnUso" }
        ],
        fixedColumns: false,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [0, 1, 2,3,4,5,6,7]
                }
            },
            {
                extend: 'excel',
                text: 'Excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7]
                }
            },
            {
                extend: 'print',
                text: 'Imprimir',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7]
                }
            }
        ]
    });

    //var TablaVehiculos = $("#GridVechiculos").DataTable({
    //    responsive: true
    //});
}
