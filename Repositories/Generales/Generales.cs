﻿using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using EEHAutomotor.Models.dbEEHAutomotor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Repositories.Generales
{
    public class Generales : IGenerales
    {
        private readonly EEHAutomotorModel _db;
        public Generales(EEHAutomotorModel mm)
        {
            _db = mm;
        }

        public void PutError(string msj, string root)
        {
            using (var _db = new EEHAutomotorModel())
            {
                var e = new BitacoraError
                {
                    Mensaje = msj,
                    Ruta = root,
                    FechaHora = DateTime.Now
                };

                _db.Entry(e).State = EntityState.Added;
                _db.SaveChanges();
            }
        }
        public List<IdValDto> GetListaTipoLicencia()
        {
            var lista = new List<string>
            {
                "Liviana Nacional",
                "Liviana Internacional",
                "Pesada No Articulada Nacional",
                "Pesada No Articulada Internacional",
                "Pesada Articulada Nacional",
                "Pesada Articulada Internacional",
                "Especial Nacional",
            };
            return lista.Select(x => new IdValDto { value = x.Trim(), label = x.Trim() }).ToList();
        }
        public List<IdValDto> GetListaCombustibles()
        {
            var lista = new List<string>
            {
                "Diesel",
                "Gasolina",
                "LPG"
            };
            return lista.Select(x => new IdValDto { value = x.Trim(), label = x.Trim() }).ToList();
        }
        public List<IdValDto> GeListaAseguradoras()
        {
            var lista = _db.Aseguradora.Distinct()
                .Select(x => new IdValDto { value = x.IdAseguradora.ToString(), label = x.Descripcion })
                .Distinct()
                .ToList();
            return lista;
        }
        public List<IdValDto> GeListaSectores()
        {
            var lista = _db.Sector.Distinct()
                .Select(x => new IdValDto { value = x.idSector.ToString(), label = x.Nombre })
                .Distinct()
                .ToList();
            return lista;
        }
        public List<IdValDto> GeListaCargo()
        {
            var lista = _db.Cargo.Distinct()
                .Select(x => new IdValDto { value = x.IdCargo.ToString(), label = x.Nombre })
                .Distinct()
                .ToList();
            return lista;
        }
        public List<IdValDto> GeListaProceso()
        {
            var lista = _db.Proceso.Distinct()
                .Select(x => new IdValDto { value = x.IdProceso.ToString(), label = x.Nombre })
                .Distinct()
                .ToList();
            return lista;
        }
    }
}
