﻿using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using EEHAutomotor.Models.dbEEHAutomotor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace EEHAutomotor.Repositories.Conductores
{
    public class Conductores : IConductores
    {
        private readonly EEHAutomotorModel _db;
        private readonly IFun _fun;
        readonly IGenerales _gen;

        public Conductores(EEHAutomotorModel mm, IFun fun, IGenerales generales)
        {
            _db = mm;
            _fun = fun;
            _gen = generales;
        }
        public ConductorDto GetConductorDtoById(string id)
        {
            var x = _db.Conductor
                .Include(x => x.IdCargoNavigation)
                .Include(x => x.IdProcesoNavigation)
                .Include(x => x.idSectorNavigation)
                .Where(x => x.IdConductor == int.Parse(_fun.DecodeBase64(id)))
                .FirstOrDefault();

            var cDto = new ConductorDto
            {
                IdConductor = x.IdConductor,
                IdEncode = _fun.EncodeBase64(x.IdConductor.ToString()),
                Identidad = x.Identidad,
                Nombre = x.Nombre,
                Telefono = x.Telefono,
                Ciudad = x.Ciudad,
                TipoLicencia = x.TipoLicencia,
                FechaLicenciaVence = x.FechaLicenciaVence,
                IdCargo = x.IdCargoNavigation?.IdCargo,
                NomCargo = x.IdCargoNavigation?.Nombre,
                IdProceso = x.IdProcesoNavigation?.IdProceso,
                NomProceso = x.IdProcesoNavigation?.Nombre,
                idSector = x.idSector,
                NomSector = x.idSectorNavigation?.Nombre
            };
            return cDto;
        }
        public List<FileDto> GetFilesDto(string id)
        {
            List<FileDto> filesDto = new List<FileDto>();

            var f = _db.LicenciaConductor
                .Where(x => x.IdConductor == int.Parse(_fun.DecodeBase64(id)))
                .ToList();

            foreach (var item in f)
            {
                var arch = new FileDto
                {
                    NomDoc = item.NomDoc,
                    Ruta = item.Ruta
                };
                filesDto.Add(arch);
            }
            return filesDto;
        }
        public List<ConductorDto> GetListaConducDto()
        {
            var lista = new List<ConductorDto>();

            var l = _db.Conductor
                .Include(x => x.IdCargoNavigation)
                .Include(x => x.IdProcesoNavigation)
                .Include(x => x.idSectorNavigation)
                .Select(x=>new ConductorDto
                {
                    IdConductor = x.IdConductor,
                    IdEncode = _fun.EncodeBase64(x.IdConductor.ToString()),
                    Identidad = x.Identidad,
                    Nombre = x.Nombre,
                    Telefono = x.Telefono,
                    Ciudad = x.Ciudad,
                    TipoLicencia = x.TipoLicencia,
                    FechaLicenciaVence = x.FechaLicenciaVence,
                    IdCargo = x.IdCargoNavigation.IdCargo,
                    NomCargo = x.IdCargoNavigation.Nombre,
                    IdProceso = x.IdProcesoNavigation.IdProceso,
                    NomProceso = x.IdProcesoNavigation.Nombre,
                    idSector = x.idSector,
                    NomSector = x.idSectorNavigation.Nombre
                })
                .ToList();
            return l;
        }
        public Message PutConductor(ConductorInsertDto cDto)
        {
            try
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<ConductorDto, Conductor>());
                var mapper = new Mapper(config);
                var Conductor = mapper.Map<Conductor>(cDto.Conductor);

                using (_db)
                {
                    if (cDto.FotosLicencia != null)
                    {
                        foreach (var f in cDto.FotosLicencia)
                        {
                            var config1 = new MapperConfiguration(cfg => cfg.CreateMap<FileDto, LicenciaConductor>());
                            var mapper1 = new Mapper(config1);
                            var foto = mapper1.Map<LicenciaConductor>(f);
                            //foto.IdConductor = Conductor.IdConductor;
                            Conductor.LicenciaConductor.Add(foto);
                        }
                        foreach (var foto in Conductor.LicenciaConductor)
                        {
                            _db.Entry(foto).State = EntityState.Added;
                        }
                    }

                    _db.Entry(Conductor).State = Conductor.IdConductor == 0 ? EntityState.Added : EntityState.Modified;

                    if (_db.SaveChanges() > 0)
                    {
                        return new Message { Code = 1, Result = "ok", Msg = "Datos guardados correctamente" };
                    }
                    else
                    {
                        return new Message { Code = 0, Result = "error", Msg = "Error en guardar los datos" };
                    }
                }
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return new Message { Code = 0, Result = "error", Msg = $"Excepción: {ex.GetBaseException().Message}" };
            }
        }

    }
}
