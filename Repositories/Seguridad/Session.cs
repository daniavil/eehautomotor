﻿using Dapper;
using EEHAutomotor.Controllers;
using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using EEHAutomotor.Models.dbEEHAutomotor;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Repositories.Seguridad
{
    public class Session : ISessionUsr
    {
        //private static readonly DbConnection ConnSQL = Fun.GetConnectionSQL("eehAppsConn");
        private readonly EEHAutomotorModel _db;
        private readonly IFun _fun;
        //private readonly IHttpContextAccessor _contextAccessor;

        public Session(EEHAutomotorModel mm, IFun fun)
        {
            _db = mm;
            _fun = fun;
        }

        public SessionUser GetSessionUser(Login login)
        {

            string sql = $@"exec [dbo].[EEHAutomotor_Login]'{login.Usuario.Trim()}', '{login.Clave.Trim()}'";
            var ListaMenu = SqlMapper.Query<RespSpAccesos>(_fun.GetConnectionSQL("dbConn"), sql, commandTimeout: 0).ToList();

            if (ListaMenu.Count > 0)
            {
                var accesos = new List<ModuloMenu>();
                foreach (var id in ListaMenu.Select(x=>x.IdModMenu).Distinct())
                {
                    var modMenu = GetModuloMenuById(id);

                    foreach (var item in modMenu.MenuAutomotor)
                    {
                        item.IdModMenuNavigation = null;
                    }
                    accesos.Add(modMenu);
                }

                var user = new SessionUser
                {
                    UsuarioSoeeh = login.Usuario.Trim(),
                    Message = new Message
                    {
                        Code = 201,
                        Result = "OK",
                        Msg = "OK"
                    },
                    MenuOpciones = new List<ModuloMenu>(accesos)
                };
                return user;
            }
            else
            {
                return null;
            }
        }

        public SessionUser GetUsrLogged()
        {
            var Usr = _fun.GetUserDecode();
            return Usr;
        }

        private ModuloMenu GetModuloMenuById(int idModMenu)
        {
            var modMenu = _db.ModuloMenu
                .Where(x => x.IdModMenu == idModMenu)
                .Include("MenuAutomotor")
                .FirstOrDefault();
            return modMenu;
        }

    }
}
