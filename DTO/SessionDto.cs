﻿using EEHAutomotor.Models.dbEEHAutomotor;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class Login
    {
        [Display(Name = "Usuario SOEEH")]
        [Required(ErrorMessage = "El Usuario es Obligatorio.")]
        public string Usuario { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "Contraseña Obligatoria.")]
        public string Clave { get; set; }

        public string LoginError { get; set; }
    }

    public class SessionUser
    {
        public string UsuarioSoeeh { get; set; }
        public Message Message { get; set; }
        public IEnumerable<ModuloMenu> MenuOpciones { get; set; }
    }

    public class RespSpAccesos
    {
        public int IDACCESO { get; set; }
        public int IdModMenu { get; set; }
        //public string NOMBRE { get; set; }
        //public string NomController { get; set; }
        //public string NomAction { get; set; }

    }

}
