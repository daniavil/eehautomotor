﻿using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Controllers
{
    public class ExcesosController : Controller
    {
        readonly IFun _fun;
        IExcesos _excesos;

        public ExcesosController(IFun fun, IExcesos excesos)
        {
            _fun = fun;
            _excesos = excesos;
        }

        // GET: ExcesosController
        public ActionResult Index()
        {
            var excesos = _excesos.GetListaExcesosDto();
            return View(excesos);
        }

        // GET: ExcesosController/Details/5
        public ActionResult Details(string id)
        {
            var e = _excesos.GetExcesosDtoByIdEncode(id);
            return View(e);
        }

        // GET: ExcesosController/Create
        public ActionResult Create()
        {
            return View();
        }

        //POST: ExcesosController/Create
       [HttpPost]
       [ValidateAntiForgeryToken]
        public ActionResult Create(ExcesoSave exceso)
        {
            var _ExcesoSave = new ExcesoSave();
            try
            {
                if (Request.Form.Files.Count==1)
                {
                    foreach (var (f, i) in Request.Form.Files.Select((v, i) => (v, i)))
                    {
                        _ExcesoSave = GetDataFromExcelFile(f);
                    }

                    if (_ExcesoSave.GuardadoOk)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        return View(_ExcesoSave);
                    }
                }
                else
                {
                    _ExcesoSave.GuardadoOk = false;
                    _ExcesoSave.MsjSave = "Archivo NO válido!";
                    return View(_ExcesoSave);
                }
            }
            catch(Exception ex)
            {
                _ExcesoSave.GuardadoOk = false;
                _ExcesoSave.MsjSave = $"Archivo NO válido! (Error: {ex.Message})";
                return View(_ExcesoSave);
            }
        }

        private ExcesoSave GetDataFromExcelFile(IFormFile file)
        {
            var Exc = new ExcesoSave();
            try
            {
                int FilasArchivo = 0;
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

                if (Exc == null || Exc.ListaExcesoDto == null)
                {
                    using (var stream = new MemoryStream())
                    {
                        file.CopyTo(stream);
                        stream.Position = 0;

                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var dataSet = reader.AsDataSet(new ExcelDataSetConfiguration
                            {
                                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                                {
                                    UseHeaderRow = true // To set First Row As Column Names    
                                }
                            });

                            if (dataSet.Tables[0].Rows.Count > 1)
                            {
                                //Conteo de filas que vienen en archivo
                                FilasArchivo = dataSet.Tables[0].Rows.Count - 1;

                                var dataTable = dataSet.Tables[0];

                                //Obtener Lista de excesos DTO y Placas Error
                                Exc = _excesos.GetListaExcesosDtoByExcel(dataTable);

                                /// Si no hay Placas Error Y
                                /// cantidades de placas en archivo con cantidad de placas DTO
                                /// son iguales, entonces procede a guardar el archivo correcto.
                                if ((FilasArchivo == Exc.ListaExcesoDto.Count) && Exc.ListaPlacaError.Count == 0)
                                {
                                    Exc = _excesos.GuardarListaExcesosVelocidad(Exc);
                                }
                                else
                                {
                                    Exc.GuardadoOk = false;
                                    Exc.MsjSave = "Error en procesar archivo";
                                }
                            }
                            else
                            {
                                Exc.GuardadoOk = false;
                                Exc.MsjSave = "Archivo no contiene registros válidos.";
                            }
                        }
                    }
                }
                return Exc;
            }
            catch (Exception ex)
            {
                Exc.GuardadoOk = false;
                Exc.MsjSave = $"Archivo NO válido! (Error: {ex.Message})";
                return Exc;
            }
        }

    }
}
