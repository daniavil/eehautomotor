﻿/*
 ***********JS Excesos**********
 *Creado Por Daniel Avila - 25/11/2021
 */

function ControlExcesos(page) {

    switch (page) {
        case 1:
            RenderIndex();
            break;
        case 2:
            RenderCreate();
            break;
        case 3:
            RenderTablaExcesosSave();
            break;
        //case 4:
        //    RenderEditVehiculo();
        //    break;
        //case 5:
        //    RenderBuscaNewPlaca();
        //    break;
        default: null
    }
}

function RenderIndex() {
    RenderTablaExcesos();
}

function RenderCreate() {
    ValidateForm('FrmNewExceso');
}

function RenderTablaExcesos() {
    var GridReportes = $("#GridReportes").DataTable({
        language: DefaultLengDatatable,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],
        columns: [
            { data: "IdExceso", visible: false},
            { data: "IdVehiculo"},
            { data: "CodPlaca" },
            { data: "Color" },
            { data: "IdMarca", visible: false },
            { data: "NomMarca" },
            { data: "Categoria" },
            { data: "Modelo" },
            { data: "VIN" },
            { data: "Motor" },
            { data: "FechaHoraFin" },
            { data: "Duracion" },
            { data: "VelocidadExceso" },
            { data: "Localizacion", visible: false },
            { data: "IdConductor", visible: false },
            { data: "NomConductor" },
            { data: "CordXY", visible: false },
            { data: "IdEncode" }
        ],
        fixedColumns: false,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                }
            },
            {
                extend: 'excel',
                text: 'Excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                }
            },
            {
                extend: 'print',
                text: 'Imprimir',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                }
            }
        ]
    });

}

function RenderTablaExcesosSave() {
    var GridReportes = $("#GridReportes").DataTable({
        language: DefaultLengDatatable,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],
        columns: [
            { data: "IdExceso", visible: false },
            { data: "IdVehiculo" },
            { data: "CodPlaca" },
            { data: "Color" },
            { data: "IdMarca", visible: false },
            { data: "NomMarca" },
            { data: "Categoria" },
            { data: "Modelo" },
            { data: "VIN" },
            { data: "Motor" },
            { data: "FechaHoraFin" },
            { data: "Duracion" },
            { data: "VelocidadExceso" },
            { data: "Localizacion" },
            { data: "IdConductor", visible: false },
            { data: "NomConductor" },
            { data: "CordXY" }
        ],
        fixedColumns: false
    });

}