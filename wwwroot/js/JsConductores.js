﻿/*
 ***********JS FLOTA**********
 *Creado Por Daniel Avila - 15/07/2021
 */

function ControlConductor(page) {

    switch (page) {
        case 1:
            RenderIndex();
            break;
        case 2:
            RenderCreate();
            break;
        case 3:
            RenderUpdate();
            break;
        //case 4:
        //    RenderEditVehiculo();
        //    break;
        //case 5:
        //    RenderBuscaNewPlaca();
        //    break;
        default: null
    }
}

function RenderIndex() {
    RenderTablaConductor();
}

function RenderCreate() {
    ValidateForm('FrmNewConduct');
}

function RenderTablaConductor() {
    var GridConductores = $("#GridConductores").DataTable({
        language: DefaultLengDatatable,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],
        columns: [
            { data: "IdConductor" },
            { data: "Identidad" },
            { data: "Nombre" },
            { data: "Telefono" },
            { data: "Ciudad" },
            { data: "NomSector" },
            { data: "TipoLicencia" },
            { data: "FechaLicenciaVence" },
            { data: "NomCargo" },
            { data: "NomProceso" },
            { data: "IdEncode" }
        ],
        fixedColumns: false,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                }
            },
            {
                extend: 'excel',
                text: 'Excel',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                }
            },
            {
                extend: 'print',
                text: 'Imprimir',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                }
            }
        ]
    });

    //var TablaVehiculos = $("#GridVechiculos").DataTable({
    //    responsive: true
    //});
}

function RenderUpdate() {
    SavePostAnyValidateForm('FrmEditConduct', '/Conductor/PutPopConductor', 0);
}