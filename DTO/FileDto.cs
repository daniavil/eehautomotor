﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class FileDto
    {
        public string NomDoc { get; set; }
        public string Ext { get; set; }
        public string Ruta { get; set; }
        public DateTime FechaCarga { get; set; }
        /// <summary>
        /// 1-> Imagen: Licencia de conductor
        /// 2-> PDF: Formato adjunto para asignación de vehículo
        /// </summary>
        public int TipoArchivo { get; set; }
    }
}
