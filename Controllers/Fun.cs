﻿using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EEHAutomotor.Controllers
{
    public class Fun: IFun
    {
        static IConfiguration _configuration;
        private readonly IHttpContextAccessor _contextAccessor;
        readonly IGenerales _gen;

        public Fun(IConfiguration configuration, IHttpContextAccessor contextAccessor, IGenerales generales)
        {
            _configuration = configuration;
            _contextAccessor = contextAccessor;
            _gen = generales;
        }

        public DbConnection GetConnectionSQL(string _conn)
        {
            try
            {
                string conStr = ConfigurationExtensions.GetConnectionString(_configuration, _conn);
                var conn = new SqlConnection(conStr);
                conn.Open();
                return conn;
            }
            catch (Exception ex)
            {
                //_gen.PutError(ex.GetBaseException().Message, ex.TargetSite.ReflectedType.FullName);
                return null;
            }
        }
        public string EncodeBase64(string value)
        {
            var valueBytes = Encoding.UTF8.GetBytes(value);
            return Convert.ToBase64String(valueBytes);
        }
        public string DecodeBase64(string value)
        {
            var valueBytes = System.Convert.FromBase64String(value);
            return Encoding.UTF8.GetString(valueBytes);
        }
        public SessionUser GetUserDecode()
        {
            try
            {
                var usuarioDecode = JsonConvert.DeserializeObject<SessionUser>
                    (DecodeBase64(_contextAccessor.HttpContext.Session.GetString("UsrSys")));
                return usuarioDecode;
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return null;
            }
            
        }
        private string RetornaDireccion(int tipo)
        {
            string filePath = "";
            switch (tipo)
            {
                case 1:
                    filePath = _configuration.GetValue<string>("DocType1");
                    break;
                case 2:
                    filePath = _configuration.GetValue<string>("DocType2");
                    break;
                case 3:
                    filePath = _configuration.GetValue<string>("DocType3");
                    break;
            }
            return filePath;
        }
        public string RetornaGeoXY(string cadena)
        {
            string Start = "["; 
            string End="]";
            try
            {
                string result = "";
                if (cadena.Contains(Start) && cadena.Contains(End))
                {
                    int StartIndex = cadena.IndexOf(Start, 0) + Start.Length;
                    int EndIndex = cadena.IndexOf(End, StartIndex);
                    result = cadena.Substring(StartIndex, EndIndex - StartIndex);
                    result = result.Replace("/", ",");
                    result = Regex.Replace(result.Trim(), @"\s+", "");
                    return result;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="tipo">1: Tipo Foto Licencias; 2: Tipo PDF archivos para asignaciones</param>
        /// <param name="NomArchivo"></param>
        /// <returns></returns>
        public FileDto SaveFile(IFormFile file,int tipo,string NomArchivo)
        {
            try
            {
                string filePath = RetornaDireccion(tipo);
                filePath = $"{filePath}\\{NomArchivo}"+ Path.GetExtension(file.FileName);

                var fileDto = new FileDto
                {
                    NomDoc = NomArchivo,//Path.GetFileNameWithoutExtension(file.FileName),
                    Ext = Path.GetExtension(file.FileName),
                    FechaCarga = DateTime.Now,
                    TipoArchivo = tipo,
                    Ruta = filePath
                };

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                return fileDto;
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return null;
            }
        }

        
    }
}
