﻿using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Components
{
    public class VCBuscaVehiculo : ViewComponent
    {
        private readonly IVehiculos _vehiculos;
        public VCBuscaVehiculo(IVehiculos vehiculos)
        {
            _vehiculos = vehiculos;
        }

        public IViewComponentResult Invoke()
        {
            var lista = _vehiculos.GetListaVehiculosDto();
            return View(lista);
        }


    }
}
