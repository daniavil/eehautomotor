﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class AsignacionVehiculo
    {
        public AsignacionVehiculo()
        {
            DocAsignacion = new HashSet<DocAsignacion>();
            ObservacionAsignacion = new HashSet<ObservacionAsignacion>();
        }

        public int IdAsignacion { get; set; }
        public int IdVehiculo { get; set; }
        public int IdConductor { get; set; }
        public DateTime FechaAsignado { get; set; }
        public DateTime? FechaDevuelve { get; set; }
        public int KmAsigna { get; set; }
        public int? KmEntrega { get; set; }
        public bool Entregado { get; set; }
        public DateTime? FechaFinaliza { get; set; }

        public virtual Conductor IdConductorNavigation { get; set; }
        public virtual Vehiculo IdVehiculoNavigation { get; set; }
        public virtual ICollection<DocAsignacion> DocAsignacion { get; set; }
        public virtual ICollection<ObservacionAsignacion> ObservacionAsignacion { get; set; }
    }
}
