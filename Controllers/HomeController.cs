﻿using EEHAutomotor.Interfaces;
using EEHAutomotor.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        readonly ISessionUsr _session;
        readonly IFun _fun;

        public HomeController(ISessionUsr session, ILogger<HomeController> logger, IFun fun)
        {
            _session = session;
            _logger = logger;
            _fun = fun;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Index()
        {

            //ViewData["NomPersona"] = _session.GetUsrLog().NombrePersona;
            //return View();
            //string cad = "25 Calle, La Esperanza, San Pedro Sula, Cortés Department, 21101, HN [15.529 / -88.017]";
            //string aaaa = _fun.RetornaGeoXY(cad);

            return View();
        }

        public IActionResult GetMenuList()
        { 
            return View();
        }

    }
}
