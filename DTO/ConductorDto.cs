﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class ConductorDto
    {
        [Key]
        public int IdConductor { get; set; }
        public string IdEncode { get; set; }
        public string Identidad { get; set; }
        public string Nombre { get; set; }
        public int? Telefono { get; set; }
        public string Ciudad { get; set; }
        public string TipoLicencia { get; set; }
        public DateTime? FechaLicenciaVence { get; set; }
        public int? IdCargo { get; set; }
        public string NomCargo { get; set; }
        public int? IdProceso { get; set; }
        public string NomProceso { get; set; }
        public int? idSector { get; set; }
        public string NomSector { get; set; }
    }


    public class ConductorInsertDto
    {
        public ConductorDto Conductor { get; set; }
        public List<FileDto> FotosLicencia { get; set; }
    }

}
