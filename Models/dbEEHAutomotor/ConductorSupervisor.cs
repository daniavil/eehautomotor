﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class ConductorSupervisor
    {
        public int IdConSup { get; set; }
        public int IdSupervisor { get; set; }
        public int IdConductor { get; set; }

        public virtual Conductor IdConductorNavigation { get; set; }
        public virtual Supervisor IdSupervisorNavigation { get; set; }
    }
}
