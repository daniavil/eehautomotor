﻿using AutoMapper;
using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using EEHAutomotor.Models.dbEEHAutomotor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Repositories.Siniestros
{
    public class Siniestros : ISiniestros
    {
        private readonly EEHAutomotorModel _db;
        private readonly IFun _fun;
        readonly IGenerales _gen;
        public Siniestros(EEHAutomotorModel mm, IFun fun, IGenerales generales)
        {
            _db = mm;
            _fun = fun;
            _gen = generales;
        }

        /// <summary>
        /// Obtener Lista de Siniestro o Siniestro específico (lista.FirstOrDefault()) por parametro IdSiniestro.
        /// </summary>
        /// <param name="IdSin"></param>
        /// <returns></returns>
        public List<SiniestroDto> GetSiniestrosDto(int? IdSin = null)
        {
            var l = _db.Siniestro
                .Include(v => v.IdVehiculoNavigation)
                .Include(p => p.IdVehiculoNavigation)
                .Include(m => m.IdVehiculoNavigation.IdMarcaNavigation)
                .Include(c => c.IdConductorNavigation)
                .Include(s => s.idSectorNavigation)
                .Where(x => (IdSin != null) ? (x.IdSiniestro == IdSin) : (x.IdSiniestro > 0))
                .Select(x => new SiniestroDto
                {
                    IdSiniestro = x.IdSiniestro,
                    IdEncode = _fun.EncodeBase64(x.IdSiniestro.ToString()),
                    IdVehiculo = x.IdVehiculo,
                    Color = x.IdVehiculoNavigation.Color,
                    IdMarca = x.IdVehiculoNavigation.IdMarca,
                    NomMarca = x.IdVehiculoNavigation.IdMarcaNavigation.Descripcion,
                    Categoria = x.IdVehiculoNavigation.Categoria,
                    ModeloDescrip = x.IdVehiculoNavigation.ModeloDescrip,
                    VIN = x.IdVehiculoNavigation.VIN,
                    Motor = x.IdVehiculoNavigation.Motor,
                    CodPlaca = x.IdVehiculoNavigation.Placa.Where(x => x.EsActual).FirstOrDefault().Codigo,
                    TarjetaCombustible = x.IdVehiculoNavigation.TarjetaCombustible,
                    IdConductor = x.IdConductor,
                    Identidad = x.IdConductorNavigation.Identidad,
                    NomConductor = x.IdConductorNavigation.Nombre,
                    Telefono = x.IdConductorNavigation.Telefono,
                    Ciudad = x.IdConductorNavigation.Ciudad,
                    FechaLicenciaVence = x.IdConductorNavigation.FechaLicenciaVence,
                    FechaOcurrio = x.FechaOcurrio,
                    FechaCreacion = x.FechaCreacion,
                    idSector = x.idSector,
                    Sector = x.idSectorNavigation.Nombre,
                    LugarEvento = x.LugarEvento,
                    ExistenTerceros = x.ExistenTerceros,
                    ExistenTercerosString = x.ExistenTerceros ? "SI" : "NO",
                    DescripDanio = x.DescripDanio,
                    RelatoSiniestro = x.RelatoSiniestro,
                    CostoReparacion = x.CostoReparacion,
                    CostoTerceros = x.CostoTerceros,
                    Responsabilidad = x.Responsabilidad,
                    VelocidadTransitada = x.VelocidadTransitada,
                    DatosTercero = x.DatosTercero,
                    UsuarioCrea = x.UsuarioCrea,
                    DocsSiniestro = (IdSin != null) ? (_db.DocSiniestro.Where(y => y.IdSiniestro == IdSin)) : (null)
                })
                .ToList();

            return l;
        }
        public Message PutSiniestro(SiniestroInsertDto sDto)
        {
            try
            {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<SiniestroDto, Siniestro>());
                var mapper = new Mapper(config);
                var sin = mapper.Map<Siniestro>(sDto.Siniestro);

                using (_db)
                {
                    if (sDto.Archivos != null)
                    {
                        foreach (var a in sDto.Archivos)
                        {
                            var config1 = new MapperConfiguration(cfg => cfg.CreateMap<FileDto, DocSiniestro>());
                            var mapper1 = new Mapper(config1);
                            var arch = mapper1.Map<DocSiniestro>(a);
                            sin.DocSiniestro.Add(arch);
                        }
                        foreach (var archivo in sin.DocSiniestro)
                        {
                            _db.Entry(archivo).State = EntityState.Added;
                        }
                    }

                    sin.FechaCreacion = DateTime.Now;
                    _db.Entry(sin).State = sin.IdSiniestro == 0 ? EntityState.Added : EntityState.Unchanged;

                    if (_db.SaveChanges() > 0)
                    {
                        return new Message { Code = 1, Result = "ok", Msg = "Datos guardados correctamente" };
                    }
                    else
                    {
                        return new Message { Code = 0, Result = "error", Msg = "Error en guardar los datos" };
                    }
                }
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return new Message { Code = 0, Result = "error", Msg = $"Excepción: {ex.GetBaseException().Message}" };
            }
        }
        public List<FileDto> GetFilesDto(string id)
        {
            List<FileDto> filesDto = new List<FileDto>();

            var f = _db.DocSiniestro
                .Where(x => x.IdSiniestro == int.Parse(_fun.DecodeBase64(id)))
                .ToList();

            foreach (var item in f)
            {
                var arch = new FileDto
                {
                    NomDoc = item.NomDoc,
                    Ruta = item.Ruta
                };
                filesDto.Add(arch);
            }
            return filesDto;
        }
        public List<IdValDto> GetListaResponsabilidad()
        {
            var lista = new List<string>
            {
                "Colaborador EEH",
                "Tercero (s)",
                "Otro"
            };
            return lista.Select(x => new IdValDto { value = x.Trim(), label = x.Trim() }).ToList();
        }
    }
}
