﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class BitacoraError
    {
        public int IdError { get; set; }
        public string Mensaje { get; set; }
        public string Ruta { get; set; }
        public DateTime FechaHora { get; set; }
    }
}
