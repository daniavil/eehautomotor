﻿/*
 ***********JS FLOTA**********
 *Creado Por Daniel Avila - 15/07/2021
 */

function ControlFlota(page) {

    switch (page) {
        case 1:
            RenderIndex();
            break;
        case 2:
            RenderCreate();
            break;
        case 3:
            RenderBuscaVehiculoVeh();
            break;
        case 4:
            RenderEditVehiculo();
            break;
        case 5:
            RenderBuscaNewPlaca();
            break;
        default: null
    }
}

function RenderIndex() {
    RenderTablaVehiculos();
}

function RenderCreate() {
    RenderCboColores();
    RenderCboCategorias();
    RenderCboModelos();

    $("#VIN").change(function () {
        ValidarDatoRepetido("VIN", 'vin');
    })
    $("#Motor").change(function () {
        ValidarDatoRepetido("Motor", 'motor');
    })

    SavePostAnyValidateForm('FrmNewVeh', '/Flota/PutPopVehiculo', 1);
}

function RenderTablaVehiculos() {
    var TablaVehiculos = $("#GridVechiculos").DataTable({
        language: DefaultLengDatatable,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],
        columns: [
            { data: "IdVehiculo"},
            { data: "Vin" },
            { data: "Motor" },
            { data: "Placa" },
            { data: "Marca" },
            { data: "Modelo" },
            { data: "Color" },
            { data: "PlacaAnterior" },
            { data: "Conductor" },
            { data: "FechaIngresoFlota" },
            { data: "KmIngreso" },
            { data: "FechaSalidaFlota" },
            { data: "KmSalida" },
            { data: "Estado" },
            { data: "IdEncode", visible: false },
            { data: "IdEncode" }
        ],
        fixedColumns: false,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                }
            },
            {
                extend: 'excel',
                text: 'Excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                }
            },
            {
                extend: 'print',
                text: 'Imprimir',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
                }
            }
        ]
    });

}

function RenderCboColores() {
    RenderSelectAutocomplete("Color", "/Flota/GetColores");
}

function RenderCboCategorias() {
    RenderSelectAutocomplete("Categoria", "/Flota/GetCategorias");
}

function RenderCboModelos() {
    RenderSelectAutocomplete("ModeloDescrip", "/Flota/GetModelos");
}

function RenderBuscaVehiculoVeh() {

    TablaVehiculos = RenderBuscaVehiculo();

    TablaVehiculos.on('click', 'button', function () {
        var data = TablaVehiculos.row($(this).parents('tr')).data();
        //var obj = { dato1: data['idCita'], dato2: data['paciente'], dato3: data['detalle'] };

        $("#IdVehiculoAnt").val(data['IdVehiculo']);
        $("#VinAnt").val(data['Vin']);
        $("#MotorAnt").val(data['Motor']);
        $("#PlacaAnt").val(data['Placa']);
        $("#MarcaAnt").val(data['Marca']);
        $("#ModeloAnt").val(data['Modelo']);
        $("#ColorAnt").val(data['Color']);
    });
}

function RenderEditVehiculo() {

    if ($('input[type=checkbox][name=Estado]').is(':checked')) {
        $("#dvSalida").hide();
    }
    else {
        $("#dvSalida").show();
    }

    $('input[type=checkbox][name=Estado]').change(function () {
        if ($(this).is(':checked')) {
            $("#dvSalida").hide();
            $("#FechaSalidaFlota").val('');
            $("#KmSalida").val('');
        }
        else {
            $("#dvSalida").show();
        }
    });

    SavePostAnyValidateForm('FrmEditVeh', '/Flota/PutPopVehiculo', 0);
}

function RenderBuscaNewPlaca() {
    var TablaNewPlaca = $("#GridNewPlacas").DataTable({
        pageLength: 10,
        columns: [
            { data: "IdPlaca", visible:false },
            { data: "Codigo" },
            {
                data: null,
                render: function (data, type, row, meta) {
                    return '<button class="btn btn-success round btn-sm" data-dismiss="modal">Seleccionar</button>';
                }
            }
        ],
        fixedColumns: false
    });

    TablaNewPlaca.on('click', 'button', function () {
        var data = TablaNewPlaca.row($(this).parents('tr')).data();
        //var obj = { dato1: data['idCita'], dato2: data['paciente'], dato3: data['detalle'] };

        $("#IdPlaca").val(data['IdPlaca']);
        $("#PlacaCod").val(data['Codigo']);

        //console.log($("#IdPlaca").val());
    });
}