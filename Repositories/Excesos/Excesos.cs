﻿using AutoMapper;
using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using EEHAutomotor.Models.dbEEHAutomotor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Repositories.Excesos
{
    public class Excesos : IExcesos
    {
        private readonly EEHAutomotorModel _db;
        private readonly IFun _fun;
        readonly IGenerales _gen;

        public Excesos(EEHAutomotorModel mm, IFun fun, IGenerales generales)
        {
            _db = mm;
            _fun = fun;
            _gen = generales;
        }

        public ExcesoDto GetExcesosDtoByIdEncode(string IdEncode)
        {
            var e = _db.HistoricoExcesoVelocidad
                .Include(x => x.IdVehiculoNavigation)
                .Include(x => x.IdVehiculoNavigation.Placa)
                .Include(x => x.IdVehiculoNavigation.IdMarcaNavigation)
                .Include(x => x.IdVehiculoNavigation.IdConductorNavigation)
                .Where(x => x.IdExceso.ToString().Equals(_fun.DecodeBase64(IdEncode)))
                .FirstOrDefault();

            var edto = new ExcesoDto()
            {
                IdExceso = Guid.NewGuid(),
                IdVehiculo = (int)e.IdVehiculo,
                CodPlaca = e.IdVehiculoNavigation.Placa.Where(x => x.EsActual).FirstOrDefault().Codigo,
                Color = e.IdVehiculoNavigation.Color,
                IdMarca = e.IdVehiculoNavigation.IdMarca,
                NomMarca = e.IdVehiculoNavigation.IdMarcaNavigation.Descripcion,
                Categoria = e.IdVehiculoNavigation.Categoria,
                Modelo = e.IdVehiculoNavigation.ModeloDescrip,
                VIN = e.IdVehiculoNavigation.VIN,
                Motor = e.IdVehiculoNavigation.Motor,
                FechaHoraFin = e.FechaHoraFin,
                Duracion = e.Duracion,
                VelocidadExceso = e.VelocidadExceso,
                Localizacion = e.Localizacion,
                IdConductor = e.IdConductor,
                Identidad = e.IdConductorNavigation.Identidad,
                NomConductor = e.IdVehiculoNavigation.IdConductorNavigation.Nombre,
                CordXY = e.CordXY,
                IdDecode = IdEncode
            };

            return edto;
        }

        public List<ExcesoDto> GetListaExcesosDto()
        {
            List<ExcesoDto> lista = new List<ExcesoDto>();
            var le = _db.HistoricoExcesoVelocidad
                .Include(x => x.IdVehiculoNavigation)
                .Include(x => x.IdVehiculoNavigation.Placa)
                .Include(x => x.IdVehiculoNavigation.IdMarcaNavigation)
                .Include(x => x.IdVehiculoNavigation.IdConductorNavigation)
                .ToList();

            foreach (var e in le)
            {
                lista.Add(new ExcesoDto()
                {
                    IdExceso = Guid.NewGuid(),
                    IdVehiculo = (int)e.IdVehiculo,
                    CodPlaca = e.IdVehiculoNavigation.Placa.Where(x=>x.EsActual).FirstOrDefault().Codigo,
                    Color = e.IdVehiculoNavigation.Color,
                    IdMarca = e.IdVehiculoNavigation.IdMarca,
                    NomMarca = e.IdVehiculoNavigation.IdMarcaNavigation.Descripcion,
                    Categoria = e.IdVehiculoNavigation.Categoria,
                    Modelo = e.IdVehiculoNavigation.ModeloDescrip,
                    VIN = e.IdVehiculoNavigation.VIN,
                    Motor = e.IdVehiculoNavigation.Motor,
                    FechaHoraFin = e.FechaHoraFin,
                    Duracion = e.Duracion,
                    VelocidadExceso = e.VelocidadExceso,
                    Localizacion = e.Localizacion,
                    IdConductor = e.IdConductor,
                    Identidad = e.IdConductorNavigation.Identidad,
                    NomConductor = e.IdVehiculoNavigation.IdConductorNavigation.Nombre,
                    CordXY = e.CordXY,
                    IdDecode = _fun.EncodeBase64(e.IdExceso.ToString())
                });
            }

            return lista;
        }

        public ExcesoSave GetListaExcesosDtoByExcel(DataTable ListaPlaca)
        {
            ExcesoSave exceso = new ExcesoSave();
            List<ExcesoDto> lista = new List<ExcesoDto>();
            List<string> pError = new List<string>();
           
            try
            {
                foreach (DataRow r in ListaPlaca.Rows)
                {

                    if (r != ListaPlaca.Rows[0])
                    {
                        var e = _db.Placa
                        .Include(x => x.IdVehiculoNavigation)
                        .Include(x => x.IdVehiculoNavigation.IdMarcaNavigation)
                        .Include(x => x.IdVehiculoNavigation.IdConductorNavigation)
                        .Where(x => x.Codigo.Equals(r["PLACA"].ToString()))
                        .FirstOrDefault();

                        if (e != null)
                        {
                            try
                            {
                                var fHoraFin = Convert.ToDateTime(r["HORAFINAL"]);
                                var Duracion = Convert.ToDateTime(r["DURACION"]);

                                lista.Add(new ExcesoDto()
                                {
                                    IdExceso = Guid.NewGuid(),
                                    IdVehiculo = (int)e.IdVehiculo,
                                    CodPlaca = e.Codigo,
                                    Color = e.IdVehiculoNavigation.Color,
                                    IdMarca = e.IdVehiculoNavigation.IdMarca,
                                    NomMarca = e.IdVehiculoNavigation.IdMarcaNavigation.Descripcion,
                                    Categoria = e.IdVehiculoNavigation.Categoria,
                                    Modelo = e.IdVehiculoNavigation.ModeloDescrip,
                                    VIN = e.IdVehiculoNavigation.VIN,
                                    Motor = e.IdVehiculoNavigation.Motor,
                                    FechaHoraFin = fHoraFin,
                                    Duracion = new DateTime(fHoraFin.Year, fHoraFin.Month, fHoraFin.Day, Duracion.Hour, Duracion.Minute, Duracion.Second),
                                    VelocidadExceso = Convert.ToInt32(r["VELOCIDADEXCESO"]),
                                    Localizacion = Convert.ToString(r["LOCALIZACION"]),
                                    IdConductor = (int)e.IdVehiculoNavigation.IdConductor,
                                    NomConductor = e.IdVehiculoNavigation.IdConductorNavigation.Nombre,
                                    CordXY = _fun.RetornaGeoXY(Convert.ToString(r["LOCALIZACION"])),
                                    IdDecode = string.Empty
                                });
                            }
                            catch (Exception ex1)
                            {
                                pError.Add(Convert.ToString(r["PLACA"]).Trim() + $"( {ex1.Message} ) ");
                            }
                        }
                        else
                        {
                            pError.Add(Convert.ToString(r["PLACA"]).Trim() + $"(Placa No encontrada) ");
                        }
                    }

                    
                }

                exceso.ListaExcesoDto = lista;
                exceso.ListaPlacaError = pError;
                return exceso;
            }
            catch (Exception ex2)
            {
                return null;
            }

        }

        public ExcesoSave GuardarListaExcesosVelocidad(ExcesoSave excesos)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ExcesoDto, HistoricoExcesoVelocidad>());
            var mapper = new Mapper(config);
            var ListExcesos = mapper.Map<List<HistoricoExcesoVelocidad>>(excesos.ListaExcesoDto);

            foreach (var r in ListExcesos)
            {
                r.Duracion = new DateTime(r.FechaHoraFin.Year, r.FechaHoraFin.Month, r.FechaHoraFin.Day, r.Duracion.Hour, r.Duracion.Minute, r.Duracion.Second);
                r.FechaHoraCarga = DateTime.Now;
            }

            try
            {
                using (_db)
                {
                    _db.AddRange(ListExcesos);
                    if (_db.SaveChanges() > 0)
                    {
                        excesos.GuardadoOk = true;
                        excesos.MsjSave = $"Se guardaron {excesos.ListaExcesoDto.Count} registros con éxito.!";
                    }
                    else
                    {
                        excesos.GuardadoOk = false;
                        excesos.MsjSave = $"Error al guardar registros.";
                    }
                }
            }
            catch (Exception ex)
            {
                excesos.GuardadoOk = false;
                excesos.MsjSave = $"Error al guardar datos: {ex.Message}";
            }
            return excesos;
        }
    }
}
