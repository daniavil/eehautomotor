﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class Sector
    {
        public Sector()
        {
            Conductor = new HashSet<Conductor>();
            Siniestro = new HashSet<Siniestro>();
        }

        public int idSector { get; set; }
        public string Nombre { get; set; }
        public int CodSector { get; set; }
        public int CodAreaInCms { get; set; }
        public int idRegion { get; set; }

        public virtual Region idRegionNavigation { get; set; }
        public virtual ICollection<Conductor> Conductor { get; set; }
        public virtual ICollection<Siniestro> Siniestro { get; set; }
    }
}
