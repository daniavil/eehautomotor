﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class LicenciaConductor
    {
        public int IdLicencia { get; set; }
        public string NomDoc { get; set; }
        public string Ruta { get; set; }
        public DateTime FechaCarga { get; set; }
        public int IdConductor { get; set; }

        public virtual Conductor IdConductorNavigation { get; set; }
    }
}
