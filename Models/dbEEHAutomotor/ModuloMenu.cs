﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class ModuloMenu
    {
        public ModuloMenu()
        {
            MenuAutomotor = new HashSet<MenuAutomotor>();
        }

        public int IdModMenu { get; set; }
        public int IdModulo { get; set; }
        public string NomModulo { get; set; }

        public virtual ICollection<MenuAutomotor> MenuAutomotor { get; set; }
    }
}
