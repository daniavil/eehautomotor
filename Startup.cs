using EEHAutomotor.Controllers;
using EEHAutomotor.Interfaces;
using EEHAutomotor.Models.dbEEHAutomotor;
using EEHAutomotor.Repositories.Asigna;
using EEHAutomotor.Repositories.Conductores;
using EEHAutomotor.Repositories.Excesos;
using EEHAutomotor.Repositories.Generales;
using EEHAutomotor.Repositories.Placas;
using EEHAutomotor.Repositories.Seguridad;
using EEHAutomotor.Repositories.Siniestros;
using EEHAutomotor.Repositories.Vehiculos;
//using EEHAutomotor.Repositories.Generales;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace EEHAutomotor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            //***************INYECTAR DEPENDENCIAS INTERFACES***************
            //services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddTransient<ISessionUsr, Session>();
            services.AddTransient<IVehiculos, Vehiculos>();
            services.AddTransient<IGenerales, Generales>();
            services.AddTransient<IPlacas, Placas>();
            services.AddTransient<IConductores, Conductores>();
            services.AddTransient<IAsigna, Asignaciones>();
            services.AddTransient<ISiniestros, Siniestros>();
            services.AddTransient<IExcesos, Excesos>();

            services.AddTransient<IFun, Fun>();
            //************************************************************

            services.AddControllersWithViews();
            services.AddRazorPages();

            // Add MVC services to the services container.
            services.AddMvc();
            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            
            //services.AddSession(Opt => {
            //    Opt.IdleTimeout = TimeSpan.FromHours(2);
            //});

            services.AddSession(Opt =>
            {
                Opt.Cookie.Name = "_aspnetCoreSession";
                Opt.IdleTimeout = TimeSpan.FromHours(2);
                Opt.Cookie.IsEssential = true;
            });


            services.AddDbContext<EEHAutomotorModel>(options => options.UseSqlServer(
                Configuration["ConnectionStrings:dbConn"]));

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Use(async (context, next) =>
            {
                context.Response.OnStarting(() =>
                {
                    if (context.Response.Headers.ContainsKey("Cache-Control"))
                    {
                        context.Response.Headers["Cache-Control"] = "no-cache,no-store";
                    }
                    else
                    {
                        context.Response.Headers.Add("Cache-Control", "no-cache,no-store");
                    }
                    if (context.Response.Headers.ContainsKey("Pragma"))
                    {
                        context.Response.Headers["Pragma"] = "no-cache";
                    }
                    else
                    {
                        context.Response.Headers.Add("Pragma", "no-cache");
                    }
                    return Task.FromResult(0);
                });
                await next.Invoke();
            });


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseStatusCodePagesWithRedirects("/EEHAutomotor/Error/{0}");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            // IMPORTANT: This session call MUST go before UseMvc()
            app.UseSession();

            app.Use(async (context, next) =>
            {
                bool isDevelopment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
                string CurrentUserIDSession = context.Session.GetString("UsrSys");
                
                if(!context.Request.Path.Value.Contains("/Login"))
                {
                    if (string.IsNullOrEmpty(CurrentUserIDSession))
                    {
                        var path = "";

                        if (isDevelopment)
                        {
                            path = $"/Login?ReturnUrl={context.Request.Path}";
                        }
                        else
                        {
                            path = $"/EEHAutomotor/Login?ReturnUrl={context.Request.Path}";
                        }
                        context.Response.Redirect(path);

                        //var path = $"Login";
                        //context.Response.Redirect(path);
                        return;
                    }

                }
                await next();
            });


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
