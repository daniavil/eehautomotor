﻿using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Controllers
{
    public class FlotaController : Controller
    {
        readonly IVehiculos _vehiculos;
        readonly IGenerales _generales;
        public FlotaController(IVehiculos vehiculos, IGenerales generales)
        {
            _vehiculos = vehiculos;
            _generales = generales;
        }
        /****************************CRUD*********************************/
        public IActionResult Index()
        {
            var ListaVehiculos = _vehiculos.GetListaVehiculosDto();
            return View(ListaVehiculos);
        }
        public IActionResult Create()
        {
            List<SelectListItem> ListMarca = new List<SelectListItem>();
            ListMarca.AddRange(new SelectList(_vehiculos.GeListatMarcaVehiculos(), "value", "label"));
            ListMarca.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["ListMarca"] = ListMarca.OrderBy(x => x.Text);

            CargaListadoCreateUpdate();
            return View();
        }

        // GET: PlacaController/Edit/5
        public IActionResult Edit(string id)
        {
            CargaListadoCreateUpdate();
            var v = _vehiculos.GetVehiculoById(id);
            return View(v);
        }

        // POST: CitaController/Put
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PutPopVehiculo(VehiculoInsertDto vehiculo)
        {
            var resp = _vehiculos.PutVehiculo(vehiculo);
            return new JsonResult(resp);
        }
        
        /*****************************************************************/
        public JsonResult GetColores()
        {
            var lista = _vehiculos.GetListaColoresVehiculos();
            return Json(lista);
        }
        public JsonResult GetCategorias()
        {
            var listaColores = _vehiculos.GetListaCategoriaVehiculos();
            return Json(listaColores);
        }
        public JsonResult GetModelos()
        {
            var lista = _vehiculos.GetListaModeloVehiculos();
            return Json(lista);
        }

        /********************************MÉTODOS*********************************/
        [HttpPost]
        public int EvalExisteVinMotorPlaca(string par)
        {
            try
            {
                string[] datos = par.Split(",");
                int IntExiste = _vehiculos.GetExisteVehiculo(datos[0].ToLower().Trim(), datos[1].ToLower().Trim()) ? 1 : 0;
                return IntExiste;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        private void CargaListadoCreateUpdate()
        {
            List<SelectListItem> Listlicencias = new List<SelectListItem>();
            Listlicencias.AddRange(new SelectList(_generales.GetListaTipoLicencia(), "value", "label"));
            Listlicencias.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["Listlicencias"] = Listlicencias.OrderBy(x => x.Text);

            List<SelectListItem> ListCombus = new List<SelectListItem>();
            ListCombus.AddRange(new SelectList(_generales.GetListaCombustibles(), "value", "label"));
            ListCombus.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["ListCombus"] = ListCombus.OrderBy(x => x.Text);

            List<SelectListItem> ListAsegura = new List<SelectListItem>();
            ListAsegura.AddRange(new SelectList(_generales.GeListaAseguradoras(), "value", "label"));
            ListAsegura.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["ListAsegura"] = ListAsegura.OrderBy(x => x.Text);
        }

    }
}
