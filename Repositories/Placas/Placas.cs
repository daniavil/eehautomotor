﻿using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using EEHAutomotor.Models.dbEEHAutomotor;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Repositories.Placas
{
    public class Placas : IPlacas
    {
        private readonly EEHAutomotorModel _db;
        readonly IGenerales _gen;
        public Placas(EEHAutomotorModel mm, IGenerales generales)
        {
            _db = mm;
            _gen = generales;
        }

        public List<PlacasDto> GetListaPlacasNuevas()
        {
            var listaDto = new List<PlacasDto>();
            var l = _db.Placa
                .Include(x=>x.IdVehiculoNavigation)
                .Include(x => x.IdVehiculoNavigation.IdMarcaNavigation)
                .Where(x => !x.EsActual && x.IdVehiculo == (int?)null)
                .ToList();

            l.ForEach(x =>
            {
                listaDto.Add(new PlacasDto
                {
                    IdPlaca = x.IdPlaca,
                    Codigo = x.Codigo,
                    EsActual = x.EsActual,
                    IdVehiculo = x.IdVehiculo,
                    Marca = x.IdVehiculoNavigation?.IdMarcaNavigation?.Descripcion,
                    Modelo = x.IdVehiculoNavigation?.ModeloDescrip,
                    Vin = x.IdVehiculoNavigation?.VIN,
                    Motor = x.IdVehiculoNavigation?.Motor,
                    Color = x.IdVehiculoNavigation?.Color
                });
            });
            return listaDto;
        }

        public List<PlacasDto> GetListaTodasPlacas()
        {
            var listaDto = new List<PlacasDto>();
            var l = _db.Placa
                .Include(x => x.IdVehiculoNavigation)
                .Include(x => x.IdVehiculoNavigation.IdMarcaNavigation)
                //.Where(x => x.EsActual && x.IdVehiculo != (int?)null)
                .ToList();

            l.ForEach(x =>
            {
                listaDto.Add(new PlacasDto
                {
                    IdPlaca = x.IdPlaca,
                    Codigo = x.Codigo,
                    EsActual = x.EsActual,
                    EnUso = x.EsActual ? "SI" : "NO",
                    IdVehiculo = x.IdVehiculo,
                    Marca = x.IdVehiculoNavigation?.IdMarcaNavigation?.Descripcion,
                    Modelo = x.IdVehiculoNavigation?.ModeloDescrip,
                    Vin = x.IdVehiculoNavigation?.VIN,
                    Motor = x.IdVehiculoNavigation?.Motor,
                    Color = x.IdVehiculoNavigation?.Color
                });
            });
            return listaDto;
        }

        public Message PutPlaca(PlacasDto placa)
        {
            try
            {
                var newPlaca = new Placa
                {
                    Codigo = placa.Codigo,
                    Correlativo = 1,
                    EsActual = false,
                    FechaCambio = DateTime.Now,
                    IdVehiculo = (int?)null
                };


                _db.Entry(newPlaca).State = EntityState.Added;
                if (_db.SaveChanges() > 0)
                {
                    return new Message { Code = 1, Result = "ok", Msg = "Datos guardados correctamente" };
                }
                else
                {
                    return new Message { Code = 0, Result = "error", Msg = "Error en guardar los datos" };
                }
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return new Message { Code = 0, Result = "error", Msg = $"Excepción: {ex.GetBaseException().Message}" };
            }
        }

    }
}
