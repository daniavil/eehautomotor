﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class Message
    {
        [Key]
        [DefaultValue(0)]
        public int Code { get; set; }
        public string Result { get; set; }
        public string Msg { get; set; }
    }
}
