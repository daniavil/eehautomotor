﻿using EEHAutomotor.Models.dbEEHAutomotor;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class ExcesoDto
    {
        [Key]
        public Guid IdExceso { get; set; } //(uniqueidentifier, not null)
        public string IdDecode { get; set; }
        public int IdVehiculo { get; set; } //(int, not null)
        public string CodPlaca { get; set; } //(nvarchar(10), not null)
        public string Color { get; set; } //(nvarchar(50), null)
        public int IdMarca { get; set; } //(int, not null)
        public string NomMarca { get; set; } //(nvarchar(50), not null)
        public string Categoria { get; set; } //(nvarchar(150), not null)
        public string Modelo { get; set; } //(nvarchar(150), not null)
        public string VIN { get; set; } //(nvarchar(100), not null)
        public string Motor { get; set; } //(nvarchar(100), null)
        public DateTime FechaHoraFin { get; set; } //(datetime, not null)
        //[DisplayFormat(DataFormatString = "{mm:ss}")]
        public DateTime Duracion { get; set; } //(datetime, not null)
        public int VelocidadExceso { get; set; } //(int, not null)
        public string Localizacion { get; set; } //(nvarchar(max), not null)
        public int IdConductor { get; set; } //(int, not null)
        public string Identidad { get; set; } //(nvarchar(200), not null)
        public string NomConductor { get; set; } //(nvarchar(200), not null)
        public string CordXY { get; set; } //(nvarchar(50), null)
    }
    public class ExcesoSave
    {
        public List<string> ListaPlacaError { get; set; }
        public List<ExcesoDto> ListaExcesoDto { get; set; }
        public bool GuardadoOk { get; set; }
        public string MsjSave { get; set; }
    }
}
