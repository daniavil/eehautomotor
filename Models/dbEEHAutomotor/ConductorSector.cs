﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class ConductorSector
    {
        public int IdConSec { get; set; }
        public int IdConductor { get; set; }
        public int idSector { get; set; }

        public virtual Conductor IdConductorNavigation { get; set; }
        public virtual Sector idSectorNavigation { get; set; }
    }
}
