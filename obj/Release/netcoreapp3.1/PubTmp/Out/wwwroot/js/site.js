﻿
var ProdPath = "";

if (!deploy) {
    ProdPath = "/EEHAutomotor";
}
console.log('Deploy: ' + deploy);

var DefaultLengDatatable = {
    sSearch: "Buscar: ",
    lengthMenu: "Mostrar _MENU_ registros por pagina",
    zeroRecords: "Ningun registro encontrado",
    info: "Pagina _PAGE_ de _PAGES_",
    infoEmpty: "Registros no disponibles",
    infoFiltered: "(filtered from _MAX_ total records)",
    oPaginate: {
        sFirst: "Primero",
        sLast: "Último",
        sNext: "Siguiente",
        sPrevious: "Anterior"
    }
};

function toastrMsj(type,Msj) {

    switch (type) {
        case 'success':
            //toastr.success(Msj, 'CORRECTO!', { positionClass: 'toast-top-full-width', containerId: 'toast-top-full-width' });
            toastr.success(Msj, 'CORRECTO!', { positionClass: 'toast-bottom-full-width', containerId: 'toast-bottom-full-width' });
            break;
        case 'warning':
            //toastr.warning(Msj, 'ATENCIÓN!', { positionClass: 'toast-top-full-width', containerId: 'toast-top-full-width' });
            toastr.warning(Msj, 'ATENCIÓN!', { positionClass: 'toast-bottom-full-width', containerId: 'toast-bottom-full-width' });
            
            break;
        case 'error':
            //toastr.error(Msj, 'ERROR!', { positionClass: 'toast-top-full-width', containerId: 'toast-top-full-width' });
            toastr.error(Msj, 'ERROR!', { positionClass: 'toast-bottom-full-width', containerId: 'toast-bottom-full-width' });
            break;
        default:
    }
};

//Limpiar Formulario
function CleanForm(frm) {
    document.getElementById(frm).reset();
}

//-------------------------------------------
/*
 * action:
 * 1 -> add
 * 0 -> Edit
 */
function SavePostAnyValidateForm(frm, url, action) {

    $(function () {
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation({
            submitError: function ($form, event, errors) {
                //event.preventDefault();
                //console.log(errors)
                //console.log(event)
                //console.log($form)
                toastrMsj('warning', 'Atención: Hay campos de datos por completar!')
            },
            submitSuccess: function ($form, event) {
                SaveGlobal(frm, url, action);
                event.preventDefault();
            },
            filter: function () {
                return $(this).is(":visible");
            }
        });
    }); 
};

function ValidateForm(frm) {

    $(function () {
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation({
            submitError: function ($form, event, errors) {
                toastrMsj('warning', 'Atención: Hay campos de datos por completar!')
            },
            submitSuccess: function ($form, event) {
                //event.preventDefault();
            },
            filter: function () {
                return $(this).is(":visible");
            }
        });
    });
};

/*
 * action:
 * 1 -> add
 * 0 -> Edit
 */
function SaveGlobal(frm, url, action) {
    //Serialize the form datas.   
    var valdata = $('#' + frm).serialize();
    //to get alert popup
    $.ajax({
        url: ProdPath + url,
        type: "POST",
        dataType: 'json',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: valdata,
        success: function (data) {
            console.log(data);

            if (data.code == '1') {
                if (action == 1) {
                    CleanForm(frm);
                }
                toastrMsj('success', data.msg);
            }
            else {
                toastrMsj('error', data.msg);
            }
        },
        error: function () {
            toastrMsj('error', 'Error en guardar los datos!');
        }
    });
    return false;
}

function RenderSelectAutocomplete(cbo,url) {
    $('#' + cbo).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: ProdPath + url,
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function (data) {
                    var array = $.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value
                        }
                    });
                    //call the filter here
                    response($.ui.autocomplete.filter(array, request.term));
                }
            });
        }
    });
}

function ValidarDatoRepetido(ctrl, tipo) {

    $.ajax({
        url: ProdPath + "/Flota/EvalExisteVinMotorPlaca",
        data: { par: tipo + ',' + $('#' + ctrl).val() },
        type: "POST",
        success: function (data) {
            if (data == '1') {
                toastrMsj('error', 'Error: El dato ' + tipo.toUpperCase() + ' ' + $('#' + ctrl).val() + ' ya existe!');
                $('#' + ctrl).val('');
            }
            else {
                toastrMsj('success', 'Dato aceptado!');
            }
        },
        error: function (xhr) {
            toastrMsj('error', 'Atención:  Ocurrió un error.!');
        }
    });
}

