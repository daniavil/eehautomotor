﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using EEHAutomotor.DTO;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class EEHAutomotorModel : DbContext
    {
        public EEHAutomotorModel()
        {
        }

        public EEHAutomotorModel(DbContextOptions<EEHAutomotorModel> options)
            : base(options)
        {
        }

        public virtual DbSet<Aseguradora> Aseguradora { get; set; }
        public virtual DbSet<AsignacionVehiculo> AsignacionVehiculo { get; set; }
        public virtual DbSet<BitacoraError> BitacoraError { get; set; }
        public virtual DbSet<Cargo> Cargo { get; set; }
        public virtual DbSet<Conductor> Conductor { get; set; }
        public virtual DbSet<DocAsignacion> DocAsignacion { get; set; }
        public virtual DbSet<DocSiniestro> DocSiniestro { get; set; }
        public virtual DbSet<HistoricoExcesoVelocidad> HistoricoExcesoVelocidad { get; set; }
        public virtual DbSet<LicenciaConductor> LicenciaConductor { get; set; }
        public virtual DbSet<Marca> Marca { get; set; }
        public virtual DbSet<MenuAutomotor> MenuAutomotor { get; set; }
        public virtual DbSet<ModuloMenu> ModuloMenu { get; set; }
        public virtual DbSet<ObservacionAsignacion> ObservacionAsignacion { get; set; }
        public virtual DbSet<Placa> Placa { get; set; }
        public virtual DbSet<Proceso> Proceso { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<Sector> Sector { get; set; }
        public virtual DbSet<Siniestro> Siniestro { get; set; }
        public virtual DbSet<Vehiculo> Vehiculo { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=192.168.100.84;Database=EEHAutomotor;persist security info=True;user id=daniel.avila;password=daniavil2402;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Aseguradora>(entity =>
            {
                entity.HasKey(e => e.IdAseguradora)
                    .HasName("PK__Asegurad__8FA1C597D3E1642A");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<AsignacionVehiculo>(entity =>
            {
                entity.HasKey(e => e.IdAsignacion);

                entity.Property(e => e.FechaAsignado).HasColumnType("date");

                entity.Property(e => e.FechaDevuelve).HasColumnType("date");

                entity.Property(e => e.FechaFinaliza).HasColumnType("date");

                entity.HasOne(d => d.IdConductorNavigation)
                    .WithMany(p => p.AsignacionVehiculo)
                    .HasForeignKey(d => d.IdConductor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AsignacionVehiculo_Conductor");

                entity.HasOne(d => d.IdVehiculoNavigation)
                    .WithMany(p => p.AsignacionVehiculo)
                    .HasForeignKey(d => d.IdVehiculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AsignacionVehiculo_Vehiculo");
            });

            modelBuilder.Entity<BitacoraError>(entity =>
            {
                entity.HasKey(e => e.IdError);

                entity.Property(e => e.FechaHora).HasColumnType("datetime");

                entity.Property(e => e.Mensaje).IsRequired();

                entity.Property(e => e.Ruta)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Cargo>(entity =>
            {
                entity.HasKey(e => e.IdCargo);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Conductor>(entity =>
            {
                entity.HasKey(e => e.IdConductor);

                entity.Property(e => e.FechaLicenciaVence).HasColumnType("date");

                entity.Property(e => e.Identidad).HasMaxLength(20);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.TipoLicencia).HasMaxLength(50);

                entity.HasOne(d => d.IdCargoNavigation)
                    .WithMany(p => p.Conductor)
                    .HasForeignKey(d => d.IdCargo)
                    .HasConstraintName("FK_Conductor_Cargo");

                entity.HasOne(d => d.IdProcesoNavigation)
                    .WithMany(p => p.Conductor)
                    .HasForeignKey(d => d.IdProceso)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Conductor_Proceso");

                entity.HasOne(d => d.idSectorNavigation)
                    .WithMany(p => p.Conductor)
                    .HasForeignKey(d => d.idSector)
                    .HasConstraintName("FK_Conductor_Sector");
            });

            modelBuilder.Entity<DocAsignacion>(entity =>
            {
                entity.HasKey(e => e.IdDocAsignacion);

                entity.Property(e => e.FechaCarga).HasColumnType("date");

                entity.Property(e => e.NomDoc)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Ruta).IsRequired();

                entity.HasOne(d => d.IdAsignacionNavigation)
                    .WithMany(p => p.DocAsignacion)
                    .HasForeignKey(d => d.IdAsignacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocAsignacion_AsignacionVehiculo");
            });

            modelBuilder.Entity<DocSiniestro>(entity =>
            {
                entity.HasKey(e => e.IdDocSiniestro);

                entity.Property(e => e.FechaCarga).HasColumnType("date");

                entity.Property(e => e.NomDoc)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Ruta).IsRequired();

                entity.HasOne(d => d.IdSiniestroNavigation)
                    .WithMany(p => p.DocSiniestro)
                    .HasForeignKey(d => d.IdSiniestro)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocSiniestro_Siniestro");
            });

            modelBuilder.Entity<HistoricoExcesoVelocidad>(entity =>
            {
                entity.HasKey(e => e.IdExceso);

                entity.Property(e => e.IdExceso).ValueGeneratedNever();

                entity.Property(e => e.CordXY).HasMaxLength(50);

                entity.Property(e => e.Duracion).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraCarga).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraFin).HasColumnType("datetime");

                entity.Property(e => e.Localizacion).IsRequired();

                entity.HasOne(d => d.IdConductorNavigation)
                    .WithMany(p => p.HistoricoExcesoVelocidad)
                    .HasForeignKey(d => d.IdConductor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HistoricoExcesoVelocidad_Conductor");

                entity.HasOne(d => d.IdVehiculoNavigation)
                    .WithMany(p => p.HistoricoExcesoVelocidad)
                    .HasForeignKey(d => d.IdVehiculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HistoricoExcesoVelocidad_Vehiculo");
            });

            modelBuilder.Entity<LicenciaConductor>(entity =>
            {
                entity.HasKey(e => e.IdLicencia);

                entity.Property(e => e.FechaCarga).HasColumnType("date");

                entity.Property(e => e.NomDoc)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Ruta).IsRequired();

                entity.HasOne(d => d.IdConductorNavigation)
                    .WithMany(p => p.LicenciaConductor)
                    .HasForeignKey(d => d.IdConductor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LicenciaConductor_Conductor");
            });

            modelBuilder.Entity<Marca>(entity =>
            {
                entity.HasKey(e => e.IdMarca);

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MenuAutomotor>(entity =>
            {
                entity.HasKey(e => e.IdMenu);

                entity.Property(e => e.NomAction)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.NomController)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.NomMenu)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.IdModMenuNavigation)
                    .WithMany(p => p.MenuAutomotor)
                    .HasForeignKey(d => d.IdModMenu)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MenuAutomotor_ModuloMenu");
            });

            modelBuilder.Entity<ModuloMenu>(entity =>
            {
                entity.HasKey(e => e.IdModMenu);

                entity.Property(e => e.NomModulo)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ObservacionAsignacion>(entity =>
            {
                entity.HasKey(e => e.IdObsAsi);

                entity.Property(e => e.Descripcion).IsRequired();

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.HasOne(d => d.IdAsignacionNavigation)
                    .WithMany(p => p.ObservacionAsignacion)
                    .HasForeignKey(d => d.IdAsignacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ObservacionAsignacion_AsignacionVehiculo");
            });

            modelBuilder.Entity<Placa>(entity =>
            {
                entity.HasKey(e => e.IdPlaca);

                entity.HasIndex(e => e.Codigo)
                    .HasName("AK_PlacaCode")
                    .IsUnique();

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.FechaCambio).HasColumnType("date");

                entity.HasOne(d => d.IdVehiculoNavigation)
                    .WithMany(p => p.Placa)
                    .HasForeignKey(d => d.IdVehiculo)
                    .HasConstraintName("FK_Placa_Vehiculo");
            });

            modelBuilder.Entity<Proceso>(entity =>
            {
                entity.HasKey(e => e.IdProceso);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.HasKey(e => e.idRegion)
                    .HasName("PK_REGION");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Sector>(entity =>
            {
                entity.HasKey(e => e.idSector)
                    .HasName("PK_SECTOR");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.idRegionNavigation)
                    .WithMany(p => p.Sector)
                    .HasForeignKey(d => d.idRegion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SECTOR_REGION");
            });

            modelBuilder.Entity<Siniestro>(entity =>
            {
                entity.HasKey(e => e.IdSiniestro);

                entity.Property(e => e.CostoReparacion).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CostoTerceros).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DescripDanio).IsRequired();

                entity.Property(e => e.FechaCreacion).HasColumnType("date");

                entity.Property(e => e.FechaOcurrio).HasColumnType("datetime");

                entity.Property(e => e.LugarEvento).IsRequired();

                entity.Property(e => e.RelatoSiniestro).IsRequired();

                entity.Property(e => e.Responsabilidad).IsRequired();

                entity.Property(e => e.UsuarioCrea)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.IdConductorNavigation)
                    .WithMany(p => p.Siniestro)
                    .HasForeignKey(d => d.IdConductor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Siniestro_Conductor");

                entity.HasOne(d => d.IdVehiculoNavigation)
                    .WithMany(p => p.Siniestro)
                    .HasForeignKey(d => d.IdVehiculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Siniestro_Vehiculo");

                entity.HasOne(d => d.idSectorNavigation)
                    .WithMany(p => p.Siniestro)
                    .HasForeignKey(d => d.idSector)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Siniestro_Sector");
            });

            modelBuilder.Entity<Vehiculo>(entity =>
            {
                entity.HasKey(e => e.IdVehiculo);

                entity.Property(e => e.Categoria)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Color).HasMaxLength(50);

                entity.Property(e => e.Combustible).HasMaxLength(50);

                entity.Property(e => e.FechaIngresoFlota).HasColumnType("date");

                entity.Property(e => e.FechaRegistro).HasColumnType("date");

                entity.Property(e => e.FechaSalidaFlota).HasColumnType("date");

                entity.Property(e => e.ModeloDescrip)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Motor).HasMaxLength(100);

                entity.Property(e => e.TarjetaCombustible).HasMaxLength(150);

                entity.Property(e => e.TipoLicencia).HasMaxLength(50);

                entity.Property(e => e.VIN)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.IdAseguradoraNavigation)
                    .WithMany(p => p.Vehiculo)
                    .HasForeignKey(d => d.IdAseguradora)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Aseguradora_Vehiculo");

                entity.HasOne(d => d.IdConductorNavigation)
                    .WithMany(p => p.Vehiculo)
                    .HasForeignKey(d => d.IdConductor)
                    .HasConstraintName("FK_Vehiculo_Conductor");

                entity.HasOne(d => d.IdMarcaNavigation)
                    .WithMany(p => p.Vehiculo)
                    .HasForeignKey(d => d.IdMarca)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Vehiculo_Marca");

                entity.HasOne(d => d.IdVehiculoAntNavigation)
                    .WithMany(p => p.InverseIdVehiculoAntNavigation)
                    .HasForeignKey(d => d.IdVehiculoAnt)
                    .HasConstraintName("FK_Vehiculo_Vehiculo");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public DbSet<EEHAutomotor.DTO.ExcesoDto> ExcesoDto { get; set; }
    }
}
