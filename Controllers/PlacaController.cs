﻿using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Controllers
{
    public class PlacaController : Controller
    {
        readonly IPlacas _placas;

        public PlacaController(IPlacas placas)
        {
            _placas = placas;
        }

        // GET: PlacaController
        public ActionResult Index()
        {
            var listaPlacas = _placas.GetListaTodasPlacas();
            return View(listaPlacas);
        }

        // GET: PlacaController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PlacaController/PutPlaca
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePlaca(PlacasDto placa)
        {
            placa.Codigo = placa.Codigo.ToUpper();
            var resp = _placas.PutPlaca(placa);
            return new JsonResult(resp);
        }
    }
}
