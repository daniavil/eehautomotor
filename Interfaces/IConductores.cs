﻿using EEHAutomotor.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Interfaces
{
    public interface IConductores
    {
        public List<ConductorDto> GetListaConducDto();
        public Message PutConductor(ConductorInsertDto cDto);
        List<FileDto> GetFilesDto(string id);
        public ConductorDto GetConductorDtoById(string id);
    }
}
