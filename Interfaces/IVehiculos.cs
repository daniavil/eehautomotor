﻿using EEHAutomotor.DTO;
using System.Collections.Generic;

namespace EEHAutomotor.Interfaces
{
    public interface IVehiculos
    {
        public List<VehiculoDto> GetListaVehiculosDto();
        public List<IdValDto> GetListaColoresVehiculos();
        public List<IdValDto> GetListaCategoriaVehiculos();
        public List<IdValDto> GetListaModeloVehiculos();
        public List<IdValDto> GeListatMarcaVehiculos();
        public List<IdValDto> GetListaConductores();
        public bool GetExisteVehiculo(string tipo, string dato);
        public Message PutVehiculo(VehiculoInsertDto veh);
        public VehiculoInsertDto GetVehiculoById(string idVeh);
        public List<VehiculoDto> GetListaVehiculoConductorNoAsignado();
    }
}
