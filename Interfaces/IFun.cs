﻿using EEHAutomotor.DTO;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Interfaces
{
    public interface IFun
    {
        public DbConnection GetConnectionSQL(string _conn);
        public string EncodeBase64(string value);
        public string DecodeBase64(string value);
        public SessionUser GetUserDecode();
        public string RetornaGeoXY(string cadena);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="tipo">1: Tipo Foto Licencias; 2: Tipo PDF archivos para asignaciones
        /// ; 3: Cualquier Tipo para subir archivos a Siniestros</param>
        /// <param name="NomArchivo"></param>
        /// <returns></returns>
        public FileDto SaveFile(IFormFile file, int tipo, string NomArchivo);
    }
}
