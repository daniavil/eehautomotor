﻿/*
 ***********JS FLOTA**********
 *Creado Por Daniel Avila - 15/10/2021
 */

function ControlSiniestros(page) {

    switch (page) {
        case 1:
            RenderIndex();
            break;
        case 2:
            RenderCreate();
            break;
        case 3:
            RenderBuscaVehiculoSin();
            RenderBuscaConductorSin();
            break;
        //case 4:
        //    RenderEditVehiculo();
        //    break;
        //case 5:
        //    RenderBuscaNewPlaca();
        //    break;
        default: null
    }
}

function RenderIndex() {
    RenderTablaSiniestros();
}

function RenderCreate() {
    //SavePostAnyValidateForm('FrmNewSiniestro', '/Siniestro/CreateSiniestro', 1);
    ValidateForm('FrmNewSiniestro');
}

function RenderTablaSiniestros() {
    var GridSiniestros = $("#GridSiniestros").DataTable({
        language: DefaultLengDatatable,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],
        columns: [
            { data: "IdSiniestro", visible: false },
            { data: "IdVehiculo" },
            { data: "Color", visible: false },
            { data: "IdMarca", visible: false },
            { data: "NomMarca" },
            { data: "Categoria", visible: false },
            { data: "ModeloDescrip", visible: false },
            { data: "VIN" },
            { data: "Motor" },
            { data: "CodPlaca" },
            { data: "TarjetaCombustible", visible: false },
            { data: "IdConductor", visible: false },
            { data: "Identidad" },
            { data: "NomConductor" },
            { data: "Telefono" },
            { data: "Ciudad", visible: false },
            { data: "FechaLicenciaVence", visible: false },
            { data: "FechaOcurrio" },
            { data: "FechaCreacion" },
            { data: "idSector", visible: false },
            { data: "Sector" },
            { data: "ExistenTerceros", visible: false },
            { data: "ExistenTercerosString" },
            { data: "DescripDanio", visible: false },
            { data: "RelatoSiniestro", visible: false },
            { data: "CostoReparacion", visible: false },
            { data: "CostoTerceros", visible: false },
            { data: "Responsabilidad", visible: false },
            { data: "VelocidadTransitada", visible: false },
            { data: "DatosTercero", visible: false },
            { data: "LugarEvento", visible: false },
            { data: "IdEncode" }
        ],
        fixedColumns: false,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [9, 17, 4, 6, 13, 12, 30, 22, 23, 25]
                }
            },
            {
                extend: 'excel',
                text: 'Excel',
                exportOptions: {
                    columns: [9, 17, 4, 6, 13, 12, 30, 22, 23, 25]
                }
            },
            {
                extend: 'print',
                text: 'Imprimir',
                exportOptions: {
                    columns: [9, 17, 4, 6, 13, 12, 30, 22, 23, 25]
                }
            }
        ]
    });
}

function RenderBuscaVehiculoSin() {

    var TablaVehiculos = RenderBuscaVehiculo();

    TablaVehiculos.on('click', 'button', function () {
        var data = TablaVehiculos.row($(this).parents('tr')).data();
        //var obj = { dato1: data['idCita'], dato2: data['paciente'], dato3: data['detalle'] };

        $("#IdVehiculo").val(data['IdVehiculo']);
        $("#VinNew").val(data['Vin']);
        $("#MotorNew").val(data['Motor']);
        $("#PlacaNew").val(data['Placa']);
        $("#MarcaNew").val(data['Marca']);
        $("#ModeloNew").val(data['Modelo']);
        $("#ColorNew").val(data['Color']);
    });
}

function RenderBuscaConductorSin() {
    var GridConductores = RenderBuscaConductor();

    GridConductores.on('click', 'button', function () {
        var data = GridConductores.row($(this).parents('tr')).data();
        $("#IdConductor").val(data['IdConductor']);
        $("#Identidad").val(data['Identidad']);
        $("#Nombre").val(data['Nombre']);
    });
}
