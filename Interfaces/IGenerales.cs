﻿using EEHAutomotor.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Interfaces
{
    public interface IGenerales
    {
        public void PutError(string msj, string root);
        public List<IdValDto> GetListaTipoLicencia();
        public List<IdValDto> GetListaCombustibles();
        public List<IdValDto> GeListaAseguradoras();
        public List<IdValDto> GeListaSectores();
        public List<IdValDto> GeListaCargo();
        public List<IdValDto> GeListaProceso();
    }
}
