﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class Conductor
    {
        public Conductor()
        {
            AsignacionVehiculo = new HashSet<AsignacionVehiculo>();
            HistoricoExcesoVelocidad = new HashSet<HistoricoExcesoVelocidad>();
            LicenciaConductor = new HashSet<LicenciaConductor>();
            Siniestro = new HashSet<Siniestro>();
            Vehiculo = new HashSet<Vehiculo>();
        }

        public int IdConductor { get; set; }
        public string Identidad { get; set; }
        public string Nombre { get; set; }
        public int? IdCargo { get; set; }
        public int? Telefono { get; set; }
        public string Ciudad { get; set; }
        public int IdProceso { get; set; }
        public string TipoLicencia { get; set; }
        public DateTime? FechaLicenciaVence { get; set; }
        public int? idSector { get; set; }

        public virtual Cargo IdCargoNavigation { get; set; }
        public virtual Proceso IdProcesoNavigation { get; set; }
        public virtual Sector idSectorNavigation { get; set; }
        public virtual ICollection<AsignacionVehiculo> AsignacionVehiculo { get; set; }
        public virtual ICollection<HistoricoExcesoVelocidad> HistoricoExcesoVelocidad { get; set; }
        public virtual ICollection<LicenciaConductor> LicenciaConductor { get; set; }
        public virtual ICollection<Siniestro> Siniestro { get; set; }
        public virtual ICollection<Vehiculo> Vehiculo { get; set; }
    }
}
