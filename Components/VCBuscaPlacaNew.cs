﻿using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Components
{
    public class VCBuscaPlacaNew : ViewComponent
    {
        private readonly IPlacas _placas;

        public VCBuscaPlacaNew(IPlacas placas)
        {
            _placas = placas;
        }

        public IViewComponentResult Invoke()
        {
            var lista = _placas.GetListaPlacasNuevas();
            return View(lista);
        }
    }
}
