﻿using EEHAutomotor.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Interfaces
{
    public interface ISessionUsr
    {
        public SessionUser GetSessionUser(Login login);
        public SessionUser GetUsrLogged();
    }
}