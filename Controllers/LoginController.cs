﻿using EEHAutomotor.Controllers;
using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSys.Controllers
{
    public class LoginController : Controller
    {
        private ISessionUsr _Session;
        private readonly IFun _fun;
        readonly IGenerales _gen;
        public LoginController(ISessionUsr session, IFun fun, IGenerales generales)
        {
            _Session = session;
            _fun = fun;
            _gen = generales;
        }

        [AllowAnonymous]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Index()
        {
            try
            {
                var usr = _Session.GetUsrLogged();

                if (usr != null)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public ActionResult LoginUser(Login login)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var resultData = _Session.GetSessionUser(login);
                    if (resultData != null)
                    {
                        string UsrJson = JsonConvert.SerializeObject(resultData);

                        if (resultData.Message.Msg == "OK")
                        {
                            //SET VARIABLE USUARIO-SESSION
                            HttpContext.Session.SetString("UsrSys", _fun.EncodeBase64(UsrJson));
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            login.LoginError = resultData.Message.Result;
                            return View("Index", login);
                        }
                    }
                    else
                    {
                        login.LoginError = "Usuario o contraseña Incorrecta";
                        return View("Index", login);
                    }
                }
                catch (Exception ex)
                {
                    login.LoginError = $"Error en Conexión: {ex}";
                    return View("Index", login);
                }
            }
            login.LoginError = "Usuario o contraseña Incorrecta";
            return View("Index", login);
        }

        [AllowAnonymous]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult LogOut()
        {
            _Session = null;
            HttpContext.Session.Remove("UsrSys");
            HttpContext.Session.Clear();

            return RedirectToAction("Index");
        }
    }
}
