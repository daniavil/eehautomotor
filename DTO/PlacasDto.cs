﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class PlacasDto
    {
        [Key]
        public int IdPlaca { get; set; }
        public string IdEncode { get; set; }
        public string Codigo { get; set; }
        public bool EsActual { get; set; }
        public string EnUso { get; set; }
        //--------------------------------
        public int? IdVehiculo { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Vin { get; set; }
        public string Motor { get; set; }
        public string Color { get; set; }
    }
}
