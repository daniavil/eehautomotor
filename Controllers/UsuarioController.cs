﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace EEHAutomotor.Controllers
{
    public class UsuarioController : Controller
    {
        //private readonly EEHAutomotorModel _db;

        //public UsuarioController(EEHAutomotorModel context)
        //{
        //    _db = context;
        //}

        // GET: Usuario
        //public async Task<IActionResult> Index()
        //{
        //    var EEHAutomotorModel = _db.Usuarios.Include(u => u.IdTipoRamaNavigation);
        //    return View(await EEHAutomotorModel.ToListAsync());
        //}

        //// GET: Usuario/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var usuarios = await _db.Usuarios
        //        .Include(u => u.IdTipoRamaNavigation)
        //        .FirstOrDefaultAsync(m => m.IdUsuario == id);
        //    if (usuarios == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(usuarios);
        //}

        //// GET: Usuario/Create
        //public IActionResult Create()
        //{
        //    ViewData["IdTipoRama"] = new SelectList(_db.TipoRama, "IdTipoRama", "Descripcion");
        //    return View();
        //}

        //// POST: Usuario/Create
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for 
        //// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("IdUsuario,Correo,Clave,Activo,IdTipoRama")] Usuarios usuarios)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _db.Add(usuarios);
        //        await _db.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["IdTipoRama"] = new SelectList(_db.TipoRama, "IdTipoRama", "Descripcion", usuarios.IdTipoRama);
        //    return View(usuarios);
        //}

        //// GET: Usuario/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var usuarios = await _db.Usuarios.FindAsync(id);
        //    if (usuarios == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["IdTipoRama"] = new SelectList(_db.TipoRama, "IdTipoRama", "Descripcion", usuarios.IdTipoRama);
        //    return View(usuarios);
        //}

        //// POST: Usuario/Edit/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for 
        //// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("IdUsuario,Correo,Clave,Activo,IdTipoRama")] Usuarios usuarios)
        //{
        //    if (id != usuarios.IdUsuario)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _db.Update(usuarios);
        //            await _db.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!UsuariosExists(usuarios.IdUsuario))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["IdTipoRama"] = new SelectList(_db.TipoRama, "IdTipoRama", "Descripcion", usuarios.IdTipoRama);
        //    return View(usuarios);
        //}

        //// GET: Usuario/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var usuarios = await _db.Usuarios
        //        .Include(u => u.IdTipoRamaNavigation)
        //        .FirstOrDefaultAsync(m => m.IdUsuario == id);
        //    if (usuarios == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(usuarios);
        //}

        //// POST: Usuario/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var usuarios = await _db.Usuarios.FindAsync(id);
        //    _db.Usuarios.Remove(usuarios);
        //    await _db.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool UsuariosExists(int id)
        //{
        //    return _db.Usuarios.Any(e => e.IdUsuario == id);
        //}
    }
}
