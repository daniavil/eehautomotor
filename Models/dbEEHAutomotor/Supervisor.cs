﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class Supervisor
    {
        public Supervisor()
        {
            ConductorSupervisor = new HashSet<ConductorSupervisor>();
        }

        public int IdSupervisor { get; set; }
        public string Nombre { get; set; }
        public int? IdCargo { get; set; }

        public virtual Cargo IdCargoNavigation { get; set; }
        public virtual ICollection<ConductorSupervisor> ConductorSupervisor { get; set; }
    }
}
