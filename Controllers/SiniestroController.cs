﻿using EEHAutomotor.DTO;
using EEHAutomotor.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Controllers
{
    public class SiniestroController : Controller
    {
        readonly ISiniestros _Siniestro;
        readonly IGenerales _gen;
        readonly IFun _fun;
        private ISessionUsr _Session;

        public SiniestroController(ISessionUsr session, ISiniestros Siniestros, IGenerales generales, IFun fun)
        {
            _Session = session;
            _Siniestro = Siniestros;
            _gen = generales;
            _fun = fun;
        }

        // GET: SiniestroController
        public ActionResult Index()
        {
            var l = _Siniestro.GetSiniestrosDto();
            return View(l);
        }

        public ActionResult Detail(string id)
        {
            var l = _Siniestro.GetSiniestrosDto(int.Parse(_fun.DecodeBase64(id)));
            //CargaListadoViewData();
            return View(l.FirstOrDefault());
        }
        public async Task<IActionResult> DownloadFilesSini(string id)
        {
            var achivos = _Siniestro.GetFilesDto(id);

            var zipFileMemoryStream = new MemoryStream();

            using (ZipArchive archive = new ZipArchive(zipFileMemoryStream, ZipArchiveMode.Update, leaveOpen: true))
            {
                foreach (var FilePath in achivos)
                {
                    var botFileName = Path.GetFileName(FilePath.Ruta);
                    var entry = archive.CreateEntry(botFileName);
                    using (var entryStream = entry.Open())
                    using (var fileStream = System.IO.File.OpenRead(FilePath.Ruta))
                    {
                        await fileStream.CopyToAsync(entryStream);
                    }
                }
            }

            zipFileMemoryStream.Seek(0, SeekOrigin.Begin);
            return File(zipFileMemoryStream, "application/octet-stream", "Archivos_Siniestro.zip");
        }

        // GET: SiniestroController/Create
        public ActionResult Create()
        {
            CargaListadoViewData();
            return View();
        }

        // POST: SiniestroController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SiniestroDto dto)
        {
            try
            {
                var archivos = new List<FileDto>();
                foreach (var (f, i) in Request.Form.Files.Select((v, i) => (v, i)))
                {
                    string nom = "File_" + (i + 1).ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    var archivo = _fun.SaveFile(f, 3, nom);
                    if (archivo != null)
                    {
                        archivos.Add(archivo);
                    }
                }

                dto.UsuarioCrea =_Session.GetUsrLogged().UsuarioSoeeh;

                var sin = new SiniestroInsertDto();
                sin.Siniestro = dto;
                sin.Archivos = new List<FileDto>(archivos);

                var res = _Siniestro.PutSiniestro(sin);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                _gen.PutError(ex.GetBaseException().Message, this.GetType().Name);
                return View();
            }
        }

        /********************************MÉTODOS*********************************/
        private void CargaListadoViewData()
        {
            List<SelectListItem> ListSector = new List<SelectListItem>();
            ListSector.AddRange(new SelectList(_gen.GeListaSectores(), "value", "label"));
            ListSector.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["ListSector"] = ListSector.OrderBy(x => x.Text);

            List<SelectListItem> ListResponsabilidad = new List<SelectListItem>();
            ListResponsabilidad.AddRange(new SelectList(_Siniestro.GetListaResponsabilidad(), "value", "label"));
            ListResponsabilidad.Insert(0, new SelectListItem { Text = "*** Seleccionar ***", Value = "" });
            ViewData["ListResponsabilidad"] = ListResponsabilidad.OrderBy(x => x.Text);
        }

    }
}
