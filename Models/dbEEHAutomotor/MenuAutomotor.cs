﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class MenuAutomotor
    {
        public int IdMenu { get; set; }
        public string NomMenu { get; set; }
        public string NomController { get; set; }
        public string NomAction { get; set; }
        public int IdAcceso { get; set; }
        public int IdModMenu { get; set; }

        public virtual ModuloMenu IdModMenuNavigation { get; set; }
    }
}
