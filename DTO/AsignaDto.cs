﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.DTO
{
    public class AsignaDto
    {
        [Key]
        public int IdAsignacion { get; set; }
        public string IdEncode { get; set; }
        public int IdVehiculo { get; set; }
        public int IdConductor { get; set; }
        public string Conductor { get; set; }
        public string Identidad { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FechaAsignado { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? FechaDevuelve { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? FechaFinaliza { get; set; }
        public int KmAsigna { get; set; }
        public int? KmEntrega { get; set; }
        public string NomSector { get; set; }
        public string Ciudad { get; set; }
        public bool Entregado { get; set; }
        public VehiculoDto Vehiculo { get; set; }
        public string ObsvInsrt { get; set; }
        public IEnumerable<ObservacionDto> Observaciones { get; set; }
}

    public class AsignaInsertDto
    {
        public AsignaDto Asigna { get; set; }
        public List<FileDto> Archivos { get; set; }
    }

    public class ObservacionDto
    {
        [Key] 
        public int IdObsAsi { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
    }

}
