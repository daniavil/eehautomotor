﻿using EEHAutomotor.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace EEHAutomotor.Interfaces
{
    public interface IExcesos
    {
        public List<ExcesoDto> GetListaExcesosDto();
        public ExcesoSave GetListaExcesosDtoByExcel(DataTable ListaExcel);
        public ExcesoSave GuardarListaExcesosVelocidad(ExcesoSave excesos);
        public ExcesoDto GetExcesosDtoByIdEncode(string IdEncode);
    }
}
