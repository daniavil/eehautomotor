﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EEHAutomotor.Models.dbEEHAutomotor
{
    public partial class Direccion
    {
        public Direccion()
        {
            Conductor = new HashSet<Conductor>();
        }

        public int IdDireccion { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Conductor> Conductor { get; set; }
    }
}
